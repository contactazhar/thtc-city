﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using THTC_CITY_NEW.Models;

namespace THTCCITYNEW.Migrations
{
    [DbContext(typeof(ApplicationDBContext))]
    partial class ApplicationDBContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.3");

            modelBuilder.Entity("THTC_CITY_NEW.Models.ContentManagement", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.ToTable("ContentManagement");
                });

            modelBuilder.Entity("THTC_CITY_NEW.Models.DelayHotspots", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("JobId");

                    b.Property<string>("JsonURL");

                    b.Property<string>("Quarter");

                    b.Property<string>("SetType");

                    b.Property<string>("Year");

                    b.Property<string>("ZipURL");

                    b.HasKey("Id");

                    b.ToTable("DelayHotspots");
                });

            modelBuilder.Entity("THTC_CITY_NEW.Models.FlowDecodedCoOrds", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CalculatedDistance");

                    b.Property<string>("Distance");

                    b.Property<string>("LongLat1");

                    b.Property<string>("LongLat1City");

                    b.Property<string>("LongLat2");

                    b.Property<string>("LongLat2City");

                    b.Property<string>("OpenLrBinary");

                    b.Property<string>("OpenLrId");

                    b.Property<string>("PName1");

                    b.Property<string>("PName2");

                    b.HasKey("Id");

                    b.ToTable("FlowDecodedCoOrds");
                });

            modelBuilder.Entity("THTC_CITY_NEW.Models.IncidentDecodedCoOrds", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LongLat1");

                    b.Property<string>("LongLat2");

                    b.Property<string>("OpenLrBinary");

                    b.Property<string>("OpenLrId");

                    b.Property<string>("PName1");

                    b.Property<string>("PName2");

                    b.HasKey("Id");

                    b.ToTable("IncidentDecodedCoOrds");
                });

            modelBuilder.Entity("THTC_CITY_NEW.Models.LiveTraffic", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Closures");

                    b.Property<int>("Jams");

                    b.Property<string>("LiveTrafficDelay");

                    b.Property<int>("LiveTrafficLevel");

                    b.Property<int>("LiveTrafficSpeed");

                    b.Property<int>("RoadWorks");

                    b.Property<DateTime>("Time");

                    b.HasKey("Id");

                    b.ToTable("LiveTraffic");
                });

            modelBuilder.Entity("THTC_CITY_NEW.Models.SideNavs", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("NavArabic");

                    b.Property<string>("NavEnglish");

                    b.HasKey("Id");

                    b.ToTable("SideNavs");
                });

            modelBuilder.Entity("THTC_CITY_NEW.Models.Test", b =>
                {
                    b.Property<string>("TestId");

                    b.Property<int>("Id");

                    b.Property<string>("MainTag");

                    b.Property<string>("Tag1");

                    b.Property<string>("Tag2");

                    b.Property<string>("Tag3");

                    b.Property<string>("UserBiography");

                    b.HasKey("TestId");

                    b.ToTable("Test");
                });
        }
    }
}
