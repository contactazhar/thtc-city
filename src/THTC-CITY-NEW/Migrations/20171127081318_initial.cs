﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace THTCCITYNEW.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ContentManagement",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentManagement", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DelayHotspots",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    JobId = table.Column<int>(nullable: false),
                    JsonURL = table.Column<string>(nullable: true),
                    Quarter = table.Column<string>(nullable: true),
                    SetType = table.Column<string>(nullable: true),
                    Year = table.Column<string>(nullable: true),
                    ZipURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelayHotspots", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FlowDecodedCoOrds",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    CalculatedDistance = table.Column<string>(nullable: true),
                    Distance = table.Column<string>(nullable: true),
                    LongLat1 = table.Column<string>(nullable: true),
                    LongLat1City = table.Column<string>(nullable: true),
                    LongLat2 = table.Column<string>(nullable: true),
                    LongLat2City = table.Column<string>(nullable: true),
                    OpenLrBinary = table.Column<string>(nullable: true),
                    OpenLrId = table.Column<string>(nullable: true),
                    PName1 = table.Column<string>(nullable: true),
                    PName2 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FlowDecodedCoOrds", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IncidentDecodedCoOrds",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    LongLat1 = table.Column<string>(nullable: true),
                    LongLat2 = table.Column<string>(nullable: true),
                    OpenLrBinary = table.Column<string>(nullable: true),
                    OpenLrId = table.Column<string>(nullable: true),
                    PName1 = table.Column<string>(nullable: true),
                    PName2 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentDecodedCoOrds", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LiveTraffic",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Closures = table.Column<int>(nullable: false),
                    Jams = table.Column<int>(nullable: false),
                    LiveTrafficDelay = table.Column<string>(nullable: true),
                    LiveTrafficLevel = table.Column<int>(nullable: false),
                    LiveTrafficSpeed = table.Column<int>(nullable: false),
                    RoadWorks = table.Column<int>(nullable: false),
                    Time = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LiveTraffic", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SideNavs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    NavArabic = table.Column<string>(nullable: true),
                    NavEnglish = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SideNavs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Test",
                columns: table => new
                {
                    TestId = table.Column<string>(nullable: false),
                    Id = table.Column<int>(nullable: false),
                    MainTag = table.Column<string>(nullable: true),
                    Tag1 = table.Column<string>(nullable: true),
                    Tag2 = table.Column<string>(nullable: true),
                    Tag3 = table.Column<string>(nullable: true),
                    UserBiography = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Test", x => x.TestId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ContentManagement");

            migrationBuilder.DropTable(
                name: "DelayHotspots");

            migrationBuilder.DropTable(
                name: "FlowDecodedCoOrds");

            migrationBuilder.DropTable(
                name: "IncidentDecodedCoOrds");

            migrationBuilder.DropTable(
                name: "LiveTraffic");

            migrationBuilder.DropTable(
                name: "SideNavs");

            migrationBuilder.DropTable(
                name: "Test");
        }
    }
}
