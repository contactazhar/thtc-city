﻿using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using THTC_CITY_NEW.Models;
using THTC_CITY_NEW.ViewModels;
using System.Xml.Serialization;
using System.Runtime.InteropServices;

namespace THTC_CITY_NEW.Controllers
{
    public class TrafficController : Controller
    {
        private readonly ApplicationDBContext _context;

        public TrafficController(ApplicationDBContext context)
        {
            _context = context;
        }


        [Route("[controller]/getSideNav")]
        [HttpPost]
        public List<SideNavs> GetSideNav()
        {

            try
            {
                List<SideNavs> sideNav = new List<SideNavs>();

                sideNav = _context.SideNavs.ToList();

                return sideNav;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/contentManagement")]
        [HttpPost]
        public List<ContentManagement> contentManagement()
        {

            try
            {
                List<ContentManagement> cntMgmt = new List<ContentManagement>();

                cntMgmt = _context.ContentManagement.ToList();

                return cntMgmt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [Route("[controller]/getTrafficFlow")]
        [HttpPost]
        public IActionResult getTrafficFlow()
        {

            try
            {
                //string URL = "https://api.tomtom.com/traffic/services/4/flowSegmentData/" + style + "/" + zoom + "/" + format + "?key=" + apiKey + "&point=" + Lat + "," + Long + "&unit=" + units;
                using (HttpClient client = new HttpClient())
                {

                    var input = "https://traffic.tomtom.com/tsq/hdf/ARE-HDF-OPENLR/a56121d7-a2bd-4426-ac03-47f34b650234/content.xml";

                    MediaTypeWithQualityHeaderValue xmlContent =
                    new MediaTypeWithQualityHeaderValue("application/xml");
                    client.DefaultRequestHeaders.Accept.Add(xmlContent);
                    HttpResponseMessage xmlResponse = client.GetAsync
                    (input).Result;

                    string xmlString = xmlResponse.Content.ReadAsStringAsync().Result;
                    using (var xReader = XmlReader.Create(new StringReader(xmlString)))
                    {
                        // This line skips the XML declaration, eg "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" - you can skip this if you don't have declaration as in your case
                        xReader.MoveToContent();
                        // Gets the actual XMLElement, if any

                        xReader.Read();

                        xReader.Skip();
                        // Convert the xReader to an XNode for the Json serializer
                        XNode node = XNode.ReadFrom(xReader);
                        // Json output
                        string jsonText = JsonConvert.SerializeXNode(node);

                        var data = JsonConvert.DeserializeObject(jsonText);
                        return Json(data);

                        //return Json("");
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/getTrafficIncident")]
        [HttpPost]
        public IActionResult getTrafficIncident(string fromWhere)
        {
            var input = "https://traffic.tomtom.com/tsq/hdt/ARE-HDT-OPENLR/a56121d7-a2bd-4426-ac03-47f34b650234/content.xml";
            using (HttpClient client = new HttpClient())
            {
                MediaTypeWithQualityHeaderValue xmlContent =
            new MediaTypeWithQualityHeaderValue("application/xml");
                client.DefaultRequestHeaders.Accept.Add(xmlContent);
                HttpResponseMessage xmlResponse = client.GetAsync
                (input).Result;

                string xmlString = xmlResponse.Content.ReadAsStringAsync().Result;
                using (var xReader = XmlReader.Create(new StringReader(xmlString)))
                {
                    // This line skips the XML declaration, eg "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" - you can skip this if you don't have declaration as in your case
                    xReader.MoveToContent();
                    // Gets the actual XMLElement, if any

                    xReader.Read();

                    xReader.Skip();
                    // Convert the xReader to an XNode for the Json serializer
                    XNode node = XNode.ReadFrom(xReader);
                    // Json output
                    string jsonText = JsonConvert.SerializeXNode(node);

                    var data = JObject.Parse(jsonText);

                    return Json(data);

                }
            }
        }

        [Route("[controller]/TrafficIncidents")]
        [HttpPost]
        public IActionResult TrafficIncidents([FromBody]List<LatLngVM> model)
        {

            try
            {
                var minX = model[0].Lat;
                var minY = model[0].Lng;
                var maxX = model[1].Lat;
                var maxY = model[1].Lng;

                var projection = "EPSG900913";
                var zoom = model[0].Zoom;
                var format = "json";
                var apiKey = "QOtGVndSDrlKfDnYjPKv9pqXDtQCKGZQ";
                var language = "en";

                var trafficModelID = getTrafficModelID(minX, minY, maxX, maxY, zoom, format, apiKey);

                using (HttpClient client = new HttpClient())
                {
                    //var incidentTime = getTrafficIncident();

                    string URL = "https://api.tomtom.com/traffic/services/4/incidentDetails/s3/" + minY + "," + minX + "," + maxY + "," + maxX + "/" + zoom + "/" + trafficModelID + "/" + "/" + format + "?key=" + apiKey + "&language=" + language + "&expandCluster=true" + "&projection=" + projection;
                    MediaTypeWithQualityHeaderValue contentType =
                        new MediaTypeWithQualityHeaderValue("application/json");
                    client.DefaultRequestHeaders.Accept.Add(contentType);
                    HttpResponseMessage response = client.GetAsync
                    (URL).Result;

                    string stringData = response.Content.ReadAsStringAsync().Result;

                    var data1 = JObject.Parse(stringData);

                    // data1["updatedTime"] = incidentTime.ToString();

                    return Json(data1);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getTrafficIncident()
        {
            var input = "https://traffic.tomtom.com/tsq/hdt/ARE-HDT-OPENLR/a56121d7-a2bd-4426-ac03-47f34b650234/content.xml";
            using (HttpClient client = new HttpClient())
            {
                MediaTypeWithQualityHeaderValue xmlContent =
            new MediaTypeWithQualityHeaderValue("application/xml");
                client.DefaultRequestHeaders.Accept.Add(xmlContent);
                HttpResponseMessage xmlResponse = client.GetAsync
                (input).Result;

                string xmlString = xmlResponse.Content.ReadAsStringAsync().Result;
                using (var xReader = XmlReader.Create(new StringReader(xmlString)))
                {
                    // This line skips the XML declaration, eg "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" - you can skip this if you don't have declaration as in your case
                    xReader.MoveToContent();
                    // Gets the actual XMLElement, if any

                    xReader.Read();

                    xReader.Skip();
                    // Convert the xReader to an XNode for the Json serializer
                    XNode node = XNode.ReadFrom(xReader);
                    // Json output
                    string jsonText = JsonConvert.SerializeXNode(node);

                    var data = JObject.Parse(jsonText);

                    var dat1 = data["payloadPublication"]["publicationTime"];
                    return dat1.ToString();


                }
            }
        }

        public string getTrafficModelID(double minX, double minY, double maxX, double maxY, int zoom, string format, string apiKey)
        {

            try
            {
                string URL = "https://api.tomtom.com/traffic/services/4/incidentViewport//" + minY + "," + minX + "," + maxY + "," + maxX + "/" + zoom + "/" + +minY + "," + minX + "," + maxY + "," + maxX + "/" + zoom + "/" + "true" + "/" + format + "?key=" + apiKey;

                using (HttpClient client = new HttpClient())
                {
                    MediaTypeWithQualityHeaderValue contentType =
                    new MediaTypeWithQualityHeaderValue("application/json");
                    client.DefaultRequestHeaders.Accept.Add(contentType);
                    HttpResponseMessage response = client.GetAsync
                    (URL).Result;

                    string stringData = response.Content.ReadAsStringAsync().Result;

                    var data = JObject.Parse(stringData);
                    var dat = data["viewpResp"]["trafficState"]["@trafficModelId"].ToString();
                    return dat;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/getTrafficData")]
        [HttpPost]
        public List<LiveTraffic> GetTrafficData()
        {

            try
            {
                List<LiveTraffic> LiveTraffic = new List<LiveTraffic>();

                LiveTraffic = _context.LiveTraffic.OrderByDescending(x => x.Id).Take(48).ToList();

                LiveTraffic = LiveTraffic.OrderBy(x => x.Id).ToList();

                foreach (var item in LiveTraffic)
                {
                    item.Time = item.Time.ToLocalTime();
                }

                return LiveTraffic;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/storeTrafficData")]
        [HttpPost]
        public async Task<LiveTraffic> StoreTrafficData([FromBody]LiveTrafficVM model)
        {

            try
            {
                LiveTraffic LiveTraffic = new LiveTraffic();

                if (model != null)
                {
                    LiveTraffic.LiveTrafficSpeed = model.LiveTrafficSpeed;
                    LiveTraffic.LiveTrafficDelay = model.LiveTrafficDelay;
                    LiveTraffic.LiveTrafficLevel = model.LiveTrafficLevel;
                    LiveTraffic.Jams = model.Jams;

                    LiveTraffic.Closures = model.Closures;
                    LiveTraffic.RoadWorks = model.RoadWorks;
                    LiveTraffic.Time = DateTime.UtcNow;

                    _context.LiveTraffic.Add(LiveTraffic);
                    await _context.SaveChangesAsync();

                }

                return LiveTraffic;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        [Route("[controller]/FlowDecoder")]
        [HttpPost]
        public async Task<List<OpenLR>> FlowDecoder([FromBody]List<OpenLR> model)
        {
            try
            {
                for (int i = 0; i < model.Count; i++)
                {
                    Process proc = null;
                    ProcessStartInfo si = null;

                    var machineName = Environment.MachineName;

                    var osVersion = RuntimeInformation.OSDescription;

                    if (osVersion.Contains("Windows") == false)
                    {
                        si = new ProcessStartInfo("otk.sh", "binview -b64 -i " + model[i].openLRData + " -k binview-line" + i + ".xml");
                    }
                    else
                    {
                        si = new ProcessStartInfo("otk.cmd", "binview -b64 -i " + model[i].openLRData + " -k binview-line" + i + ".xml");
                    }

                    // set the working directory to the location of the the legacy program
                    si.WorkingDirectory = Directory.GetCurrentDirectory();

                    // start a new process using the start info object
                    proc = Process.Start(si);

                    // wait for the process to complete before continuing
                    proc.WaitForExit();

                    var input = Directory.GetCurrentDirectory() + "\\binview-line" + i + ".xml";

                    using (var xReader = XmlReader.Create(input))
                    {
                        // This line skips the XML declaration, eg "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" - you can skip this if you don't have declaration as in your case
                        xReader.MoveToContent();
                        // Gets the actual XMLElement, if any

                        xReader.Read();

                        xReader.Skip();
                        // Convert the xReader to an XNode for the Json serializer
                        XNode node = XNode.ReadFrom(xReader);
                        // Json output
                        string jsonText = JsonConvert.SerializeXNode(node);

                        var data = JObject.Parse(jsonText);
                        var coordinate1 = data["Document"]["Folder"]["Placemark"][0]["Point"]["coordinates"];
                        var coordinate2 = data["Document"]["Folder"]["Placemark"][1]["Point"]["coordinates"];

                        var PName1 = data["Document"]["Folder"]["Placemark"][0]["name"];
                        var PName2 = data["Document"]["Folder"]["Placemark"][1]["name"];

                        string description = (string)data["Document"]["Folder"]["Placemark"][0]["description"];
                        var subStrings = description.Split(':');
                        int index = subStrings[7].IndexOf('m');
                        var distance = string.Empty;
                        if (index > 0)
                        {
                            distance = subStrings[7].Substring(0, index);
                        }

                        var CoOrds = new List<string>();
                        CoOrds.Add(coordinate1.ToString());
                        CoOrds.Add(coordinate2.ToString());
                        model[i].returnData = CoOrds;

                        FlowDecodedCoOrds flowCoOrds = new FlowDecodedCoOrds();

                        flowCoOrds.OpenLrId = model[i].openLRId;
                        flowCoOrds.OpenLrBinary = model[i].openLRData;
                        flowCoOrds.LongLat1 = coordinate1.ToString();
                        flowCoOrds.LongLat2 = coordinate2.ToString();
                        flowCoOrds.PName1 = PName1.ToString();
                        flowCoOrds.PName2 = PName2.ToString();
                        flowCoOrds.Distance = distance;
                        _context.FlowDecodedCoOrds.Add(flowCoOrds);
                        await _context.SaveChangesAsync();
                    }
                }

                return model;


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        [Route("[controller]/IncidentDecoder")]
        [HttpPost]
        public async Task<List<OpenLR>> IncidentDecoder([FromBody]List<OpenLR> model)
        {


            try
            {
                for (int i = 0; i < model.Count; i++)
                {
                    Process proc = null;
                    ProcessStartInfo si = null;

                    var machineName = Environment.MachineName;

                    var osVersion = RuntimeInformation.OSDescription;

                    if (osVersion.Contains("Windows") == false)
                    {
                        si = new ProcessStartInfo("otk.sh", "binview -b64 -i " + model[i].openLRData + " -k binview-line" + i + ".xml");
                    }
                    else
                    {
                        si = new ProcessStartInfo("otk.cmd", "binview -b64 -i " + model[i].openLRData + " -k binview-line" + i + ".xml");
                    }

                    // set the working directory to the location of the the legacy program
                    si.WorkingDirectory = Directory.GetCurrentDirectory();

                    // start a new process using the start info object
                    proc = Process.Start(si);

                    // wait for the process to complete before continuing
                    proc.WaitForExit();

                    var input = si.WorkingDirectory + "\\binview-line" + i + ".xml";

                    using (var xReader = XmlReader.Create(input))
                    {
                        // This line skips the XML declaration, eg "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" - you can skip this if you don't have declaration as in your case
                        xReader.MoveToContent();
                        // Gets the actual XMLElement, if any

                        xReader.Read();

                        xReader.Skip();
                        // Convert the xReader to an XNode for the Json serializer
                        XNode node = XNode.ReadFrom(xReader);
                        // Json output
                        string jsonText = JsonConvert.SerializeXNode(node);

                        var data = JObject.Parse(jsonText);
                        var coordinate1 = data["Document"]["Folder"]["Placemark"][0]["Point"]["coordinates"];
                        var coordinate2 = data["Document"]["Folder"]["Placemark"][1]["Point"]["coordinates"];

                        var PName1 = data["Document"]["Folder"]["Placemark"][0]["name"];
                        var PName2 = data["Document"]["Folder"]["Placemark"][1]["name"];


                        var CoOrds = new List<string>();
                        CoOrds.Add(coordinate1.ToString());
                        CoOrds.Add(coordinate2.ToString());
                        model[i].returnData = CoOrds;

                        IncidentDecodedCoOrds incidentCoOrds = new IncidentDecodedCoOrds();

                        incidentCoOrds.OpenLrId = model[i].openLRId;
                        incidentCoOrds.OpenLrBinary = model[i].openLRData;
                        incidentCoOrds.LongLat1 = coordinate1.ToString();
                        incidentCoOrds.LongLat2 = coordinate2.ToString();
                        incidentCoOrds.PName1 = PName1.ToString();
                        incidentCoOrds.PName2 = PName2.ToString();

                        _context.IncidentDecodedCoOrds.Add(incidentCoOrds);

                        await _context.SaveChangesAsync();

                    }
                }

                return model;


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        [Route("[controller]/flowDecodedResults")]
        [HttpPost]
        public async Task<List<FlowDecodedCoOrds>> flowDecodedResults([FromBody]List<FlowDecodedCoOrdsVM> model)
        {

            try
            {
                var flowCoOrds = new List<FlowDecodedCoOrds>();

                if (model.Count == 0)
                {

                    await Task.Run(() => flowCoOrds = _context.FlowDecodedCoOrds.ToList());
                }
                else
                {
                    await Task.Run(() => flowCoOrds = _context.FlowDecodedCoOrds.ToList());
                    if (flowCoOrds.Count == model.Count)
                    {
                        for (int i = 0; i < flowCoOrds.Count; i++)
                        {
                            if (flowCoOrds[i].OpenLrId == model[i].OpenLrId)
                            {
                                flowCoOrds[i].LongLat2City = model[i].City;
                                flowCoOrds[i].CalculatedDistance = model[i].CalculatedDistance;
                            }
                            //flowCoOrds[i].LongLat1City = model[i].LongLat1City;
                            //flowCoOrds[i].LongLat2City = model[i].LongLat2City;
                        }
                        await _context.SaveChangesAsync();
                    }
                }

                return flowCoOrds;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        [Route("[controller]/storeDelayHotspots")]
        [HttpPost]
        public async Task<IActionResult> storeDelayHotspots([FromBody]DelayHotspotsVM model)
        {
            var delayHotspot = new DelayHotspots();
            await Task.Run(() => delayHotspot = _context.DelayHotspots.Where(x => x.Year == model.Year && x.Quarter == model.Quarter && x.SetType == model.SetType).FirstOrDefault());
            try
            {

                if (delayHotspot == null)
                {
                    delayHotspot = new DelayHotspots();

                    delayHotspot.JobId = model.JobId;
                    delayHotspot.JsonURL = model.JsonURL;
                    delayHotspot.ZipURL = model.ZipURL;
                    delayHotspot.Year = model.Year;
                    delayHotspot.Quarter = model.Quarter;
                    delayHotspot.SetType = model.SetType;

                    _context.Add(delayHotspot);
                    await _context.SaveChangesAsync();

                    return Json(model);

                }
                else if (delayHotspot != null && delayHotspot.JobId == model.JobId && model.JsonURL != null)
                {
                    delayHotspot.JsonURL = model.JsonURL;
                    delayHotspot.ZipURL = model.ZipURL;

                    await _context.SaveChangesAsync();
                    return Json(model);
                }
                return Json("exist");


            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        [Route("[controller]/getDelayHotspotsByYearQuarterAndType")]
        [HttpPost]
        public async Task<IActionResult> getDelayHotspotsByYearQuarterAndType([FromBody]DelayHotspotsVM model)
        {
            try
            {
                var delayHotspot = new DelayHotspots();
                await Task.Run(() => delayHotspot = _context.DelayHotspots.Where(x => x.Year == model.Year && x.Quarter == model.Quarter && x.SetType == model.SetType).FirstOrDefault());

                return Json(delayHotspot);

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        [Route("[controller]/getQuartersByYear")]
        [HttpPost]
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task<IActionResult> getQuartersByYear([FromBody]DelayHotspotsVM model)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {

            try
            {
                var quarters = _context.DelayHotspots.Where(x => x.Year == model.Year && x.JsonURL != null).GroupBy(x => x.Quarter);
                return Json(quarters);

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        [Route("[controller]/getYears")]
        [HttpGet]
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task<IActionResult> getYears()
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {

            try
            {
                var years = _context.DelayHotspots.GroupBy(x => x.Year);
                return Json(years);

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        [Route("[controller]/getBasesetByYearQuarter")]
        [HttpPost]
        public async Task<IActionResult> getBasesetByYearQuarter([FromBody]DelayHotspotsVM model)
        {

            try
            {
                var delayHotspot = new DelayHotspots();
                await Task.Run(() => delayHotspot = _context.DelayHotspots.Where(x => x.Year == model.Year && x.Quarter == model.Quarter && x.SetType == model.SetType).FirstOrDefault());
                return Json(delayHotspot);

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        [Route("[controller]/getCAAWithoutURLs")]
        [HttpGet]
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task<IActionResult> getCAAWithoutURLs()
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {

            try
            {
                var caaWihtoutURLs = _context.DelayHotspots.Where(x => x.JsonURL == null).ToList();
                return Json(caaWihtoutURLs);

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }


    }
}
