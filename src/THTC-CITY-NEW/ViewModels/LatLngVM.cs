﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace THTC_CITY_NEW
{
    public class LatLngVM
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int Zoom { get; set; }
        public DateTime lastUpdatedTime { get; set; }
        public bool isAccessFromLayout { get; set; }
        public string Language { get; set; }


    }

    public class PointVM
    {
        public double X { get; set; }
        public double Y { get; set; }
    }
}
