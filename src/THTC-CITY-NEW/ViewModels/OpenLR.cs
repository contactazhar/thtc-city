﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace THTC_CITY_NEW.ViewModels
{
    public class OpenLR
    {
        public string openLRId { get; set; }
        public string openLRData { get; set; }

        public object returnData { get; set; }
    }
}
