﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using THTC_CITY_NEW.Models;

namespace THTC_CITY_NEW.ViewModels
{
    public class DelayHotspotsVM
    {
        public int Id { get; set; }

        public string Year { get; set; }

        public string Quarter { get; set; }

        public string SetType { get; set; }

        public int JobId { get; set; }

        public string JsonURL { get; set; }

        public string ZipURL { get; set; }
    }

}
