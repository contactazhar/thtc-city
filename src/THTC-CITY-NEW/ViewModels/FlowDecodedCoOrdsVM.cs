﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace THTC_CITY_NEW.ViewModels
{
    public class FlowDecodedCoOrdsVM
    {
        public int Id { get; set; }
        public string OpenLrBinary { get; set; }
        public string OpenLrId { get; set; }
        public string PName1 { get; set; }
        public string LongLat1 { get; set; }
        public string PName2 { get; set; }
        public string LongLat2 { get; set; }
        public string Long1 { get; set; }
        public string Lat1 { get; set; }
        public string Long2 { get; set; }
        public string Lat2 { get; set; }
        public string City { get; set; }
        public string CalculatedDistance { get; set; }
    }
}
