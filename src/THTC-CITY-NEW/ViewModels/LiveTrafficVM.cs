﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace THTC_CITY_NEW.ViewModels
{
    public class LiveTrafficVM
    {
        public int Id { get; set; }

        public int LiveTrafficSpeed { get; set; }

        public int LiveTrafficLevel { get; set; }

        public int RoadWorks { get; set; }

        public int Jams { get; set; }

        public int Closures { get; set; }

        public string LiveTrafficDelay { get; set; }

        public DateTime Time { get; set; }

        public List<LatLngVM> LatLng { get; set; }
    }
}
