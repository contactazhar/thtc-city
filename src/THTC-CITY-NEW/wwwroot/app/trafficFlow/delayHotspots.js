﻿(function () {
    'use strict';

    angular
        .module('trafficApp')
        .controller('delayHotspots', delayHotspots);

    delayHotspots.$inject = ['$scope', '$http', 'orderByFilter', '$timeout'];

    function delayHotspots($scope, $http, orderBy, $timeout) {
        $scope.title = 'delayHotspots';
        $scope.selectedTab = 'delayHotspots';
        $scope.sideNav = [];
        var basePath = '/';
        var map;
        var markers = [];
        $scope.showSpinner = true;

        $scope.selectedQuarter = "Q1";

        $scope.selectedYear = '2017';

        $scope.delayHotspots = [];

        var vectorLayer = new OpenLayers.Layer.Vector("myLayer");


        var fromProjection = new OpenLayers.Projection("EPSG:4326"); // transform from WGS 1984
        var toProjection = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection


        activate();

        $scope.selecteLanguage = window.localStorage.getItem('language');
        $scope.Content = JSON.parse(window.localStorage.getItem('content'));
        $scope.sideNav = JSON.parse(window.localStorage.getItem('sideNav'));

        if ($scope.selecteLanguage == 'en') {
            $scope.selectedTiming = 'MORNING';
            $scope.Timings = ['MORNING', 'AFTERNOON', 'EVENING'];
        } else {
            $scope.selectedTiming = 'صباح';
            $scope.Timings = ['صباح', 'بعد الظهر', 'مساء'];
        }



        function activate() {

            setTimeout(function () {
                if ($scope.selecteLanguage == null || $scope.selecteLanguage == undefined) {
                    window.location.reload();
                }
                initMap();
                //  getSideNav();
                getDelayHotspots();
                $(".nano").nanoScroller();
                getTimeAndDate();

            }, 1000);


        }

        function getDelayHotspots() {
            $http({
                method: 'GET',
                url: basePath + 'Traffic/getYears',
                headers: "application/json"
            }).then(function (data) {
                $scope.Years = [];
                $scope.resultYears = data.data;
                for (var i = 0; i < $scope.resultYears.length; i++) {
                    $scope.Years.push($scope.resultYears[i][0].year);
                }
            });
            $scope.getQuartersByYear($scope.selectedYear);
        }

        function getSideNav() {

            $http({
                method: 'POST',
                url: basePath + 'Traffic/getSideNav',
                headers: "application/json"
            }).then(function (data) {
                $scope.sideNav = data.data;
                window.localStorage.setItem('showList', JSON.stringify($scope.sideNav[36]));
                window.localStorage.setItem('hideList', JSON.stringify($scope.sideNav[37]));
            });

        }

        function getTimeAndDate() {

            var month = new Array();
            month[0] = "Jan";
            month[1] = "Feb";
            month[2] = "Mar";
            month[3] = "Apr";
            month[4] = "May";
            month[5] = "Jun";
            month[6] = "Jul";
            month[7] = "Aug";
            month[8] = "Sep";
            month[9] = "Oct";
            month[10] = "Nov";
            month[11] = "Dec";

            var currentDate = new Date;

            var day = currentDate.getDate();
            var months = month[currentDate.getMonth()];
            var year = currentDate.getFullYear();
            var time = new Date(currentDate.setMinutes(currentDate.getMinutes() - 90)).toLocaleTimeString();

            $scope.currentDate = {};
            $scope.currentDate.day = day;
            $scope.currentDate.month = months;
            $scope.currentDate.year = year;
            $scope.currentDate.time = time;
        }


        function initMap() {
            // create the maps
            var extent = new OpenLayers.Bounds(Number($scope.Content[0].value), Number($scope.Content[1].value), Number($scope.Content[2].value), Number($scope.Content[3].value)).transform(fromProjection, toProjection);
            var offset = Number($scope.Content[4].value); // control max zoom-out level

            var options = {
                resolution: Number($scope.Content[5].value),
                numZoomLevels: Number($scope.Content[6].value) - offset, // control zoom-in level
                maxResolution: Number($scope.Content[7].value) / Math.pow(2, offset),
                maxExtent: new OpenLayers.Bounds(Number($scope.Content[8].value), Number($scope.Content[9].value), Number($scope.Content[10].value), Number($scope.Content[11].value)),
                projection: "EPSG:900913",
                units: 'm',
                format: $scope.Content[15].value,
                restrictedExtent: extent,
                eventListeners: {
                    "click": clearPopup
                }
            };

            map = new OpenLayers.Map("map3", options);

            if ($scope.selecteLanguage == 'en') {
                var layers = $scope.Content[13].value;
            } else {
                var layers = $scope.Content[48].value;
            }

            var wms_th = new OpenLayers.Layer.WMS(
                 $scope.Content[12].value,
                 $scope.Content[25].value,
                   {
                       sphericalMercator: true,
                       layers: layers,
                       tiled: $scope.Content[14].value,
                       format: $scope.Content[15].value,
                       tilesOrigin: map.maxExtent.left + ',' + map.maxExtent.bottom
                   },
                   {
                       buffer: 0,
                       isBaseLayer: true,

                       displayOutsideMaxExtent: true
                   }
                  );

            var flowUrl = $scope.Content[26].value;


            var TTFlow = new OpenLayers.Layer.XYZ("Flow Layer", flowUrl, { zoomOffset: offset, isBaseLayer: false });

            var markers = new OpenLayers.Layer.Markers("Markers", { zoomOffset: offset, isBaseLayer: false });

            var labelUrl = $scope.Content[17].value;

            var nameLayerTTFlow = new OpenLayers.Layer.XYZ("Label Layer", labelUrl, { zoomOffset: offset, isBaseLayer: false });


            if ($scope.selecteLanguage == 'en') {
                map.addLayers([TTFlow, wms_th, nameLayerTTFlow, markers]);
            } else {
                map.addLayers([TTFlow, wms_th, markers]);
            }

            if ($scope.Content[24].value === 'dubai') {
                var zoom = Number($scope.Content[49].value) - 1;
            } else {
                var zoom = Number($scope.Content[49].value);
            }
            map.zoomToMaxExtent();
            map.setCenter(
               new OpenLayers.LonLat($scope.Content[18].value, $scope.Content[19].value).transform(fromProjection, toProjection),
               zoom
           );

            $scope.map = map;

            //debugger
            //var SW = new OpenLayers.LonLat(54.852, 24.449).transform(fromProjection, toProjection);
            //var NE = new OpenLayers.LonLat(55.679, 25.620).transform(fromProjection, toProjection);

        }



        function clearMarkers() {

            var markersLayer = map.getLayersByName('Markers')[0];
            markersLayer.clearMarkers();

        }
        function hidePopups() {

            for (var i = 0; i < map.popups.length; i++) {
                if (map.popups[i].visible()) {
                    map.popups[i].hide();
                }
            };
        }

        function clearPopup() {
            var length = map.popups.length;
            for (var i = 0; i < length; i++) {
                if (map.popups[i].visible() == true) {
                    map.popups[i].hide();
                }
            }
        }

        $scope.drawPopup = function (map, latlng, contentString, title, icon) {

            if (map === undefined) {
                map = $scope.map;
            }

            if ($scope.selecteLanguage == 'ar') {
                var size = new OpenLayers.Size(350, 180);
            } else {
                var size = new OpenLayers.Size(280, 180);
            }

            if (this.popup === null || this.popup === undefined) {
                hidePopups();
                this.popup = new OpenLayers.Popup.Anchored(title,
                new OpenLayers.LonLat(latlng.lng, latlng.lat).transform(fromProjection, toProjection),
                size,
                contentString,
                null,
                true);

                map.addPopup(this.popup);
                this.popup.show();
                map.setCenter(
                new OpenLayers.LonLat(latlng.lng, latlng.lat).transform(fromProjection, toProjection),
                5
            );
            } else {
                if (this.popup.visible()) {
                    if ($scope.Content[24].value === 'dubai') {
                        var zoom = Number($scope.Content[49].value) - 1;
                    } else {
                        var zoom = Number($scope.Content[49].value);
                    }
                    Number($scope.Content[49].value) - 1
                    hidePopups();
                    map.setCenter(
                     new OpenLayers.LonLat($scope.Content[18].value, $scope.Content[19].value).transform(fromProjection, toProjection),
                     zoom
                    );
                } else {
                    hidePopups();
                    this.popup.show();
                    map.setCenter(
                    new OpenLayers.LonLat(latlng.lng, latlng.lat).transform(fromProjection, toProjection),
                    5
                    );
                }


            }
            this.popup.autoSize = true;
        }

        $scope.drawMarker = function (map, latlng, contentString, title, icon) {

            var size = new OpenLayers.Size(40, 50);
            var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
            var icon = new OpenLayers.Icon(window.location.origin + '/' + icon, 48, offset);
            var marker = new OpenLayers.Marker(new OpenLayers.LonLat(latlng.lng, latlng.lat).transform(fromProjection, toProjection), icon.clone());
            marker.setOpacity(1);
            marker.events.register('click', marker, function (evt) {



                if ($scope.selecteLanguage == 'ar') {
                    var size = new OpenLayers.Size(350, 180);
                } else {
                    var size = new OpenLayers.Size(280, 180);
                }
                if (this.popup === null || this.popup === undefined) {
                    hidePopups();
                    this.popup = new OpenLayers.Popup.Anchored(title,
                    marker.lonlat,
                    size,
                    contentString,
                    null,
                    true);

                    map.addPopup(this.popup);
                    this.popup.show();
                } else {

                    if (this.popup.visible()) {
                        hidePopups();
                    } else {
                        hidePopups();
                        this.popup.show();
                    }


                }
                this.popup.autoSize = true;
            });

            var markersLayer = map.getLayersByName('Markers')[0];
            markersLayer.addMarker(marker);
            map.raiseLayer(markersLayer, map.layers.length);
        }
        $scope.getQuartersByYear = function (year) {

            $scope.prevYear = $scope.selectedYear;
            $scope.year = {
            };
            $scope.year.year = year;
            $http({
                method: 'POST',
                url: basePath + 'Traffic/getQuartersByYear',
                headers: "application/json",
                data: $scope.year
            }).then(function (data) {
                $scope.Results = data.data;
                $scope.Quarters = [];
                for (var i = 0; i < $scope.Results.length; i++) {
                    $scope.Quarters.push($scope.Results[i]["0"].quarter);
                }
                if ($scope.selectedQuarter != $scope.Quarters["0"]) {
                    $scope.selectedQuarter = $scope.Quarters["0"];
                }
                var quarter = $scope.selectedQuarter;
                if (year != null || year != undefined) {
                    $scope.getBasesetByYearQuarter(quarter, year);
                }

            });
        }


        $scope.getBasesetByYearQuarter = function (selectedQuarter, selectedYear) {
            //Baseset
            $scope.showSpinner = true;
            $scope.getBasesetData = {
            };
            if (selectedYear == null || selectedYear == undefined) {
                $scope.getBasesetData.year = $scope.selectedYear;

            } else {
                $scope.getBasesetData.year = selectedYear;
                $scope.selectedYear = selectedYear;
            }

            if (selectedQuarter == null || selectedQuarter == undefined) {
                $scope.getBasesetData.quarter = $scope.selectedQuarter;
            } else {
                $scope.getBasesetData.quarter = selectedQuarter;
                $scope.selectedQuarter = selectedQuarter;
            }

            $scope.getBasesetData.setType = 'BaseSet';

            $http({
                async: true,
                crossDomain: true,
                method: 'POST',
                url: basePath + 'Traffic/getBasesetByYearQuarter',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.getBasesetData
            }).then(function (data) {

                $scope.baseSet = data.data;

                if (($scope.baseSet == null || $scope.baseSet == undefined) && $scope.prevYear == $scope.selectedYear) {
                    if ($scope.selecteLanguage == 'en') {

                        alert($scope.sideNav[55].navEnglish);
                    } else {
                        alert($scope.sideNav[55].navArabic);

                    }

                } else {

                    if ($scope.baseSet != null) {


                        $http({
                            crossDomain: true,
                            method: 'GET',
                            url: $scope.baseSet.jsonURL,
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        }).then(function (result) {

                            $scope.baseSetData = result.data;
                            var length = $scope.baseSetData.network.segmentResults.length;

                            $scope.getDelayHotspotByYearQuarterAndType(null, $scope.getBasesetData.quarter, $scope.getBasesetData.year);

                        }).catch(function (error) {
                            //console.log('Cant get data');
                            console.log(error);

                        });

                    }
                }

            }).catch(function (data) {
                console.log(data);
            });
        }


        $scope.getDelayHotspotByYearQuarterAndType = function (selectedTiming, selectedQuarter, selectedYear) {
            var format = new OpenLayers.Format.WKT({
                'internalProjection': map.baseLayer.projection,
                'externalProjection': new OpenLayers.Projection("EPSG:4326")
            });

            vectorLayer.destroyFeatures();
            clearPopup();
            if ($scope.Content[24].value === 'dubai') {
                var zoom = Number($scope.Content[49].value) - 1;
            } else {
                var zoom = Number($scope.Content[49].value);
            }

            map.setCenter(
               new OpenLayers.LonLat($scope.Content[18].value, $scope.Content[19].value).transform(fromProjection, toProjection),
               zoom
           );
            $scope.getDelayHotspotsData = {
            };
            $scope.getDelayHotspotsData.Year = selectedYear;
            $scope.getDelayHotspotsData.Quarter = selectedQuarter;
            $scope.selectedQuarter = selectedQuarter;
            if (selectedTiming != null || selectedTiming != undefined) {

                $scope.selectedTiming = selectedTiming;
                }

            if ($scope.selecteLanguage == 'ar') {
                if ($scope.selectedTiming == 'صباح') {
                    $scope.selectedTiming = 'MORNING';
                    } else if ($scope.selectedTiming == 'بعد الظهر') {
                        $scope.selectedTiming = 'AFTERNOON';
                        } else if ($scope.selectedTiming == 'مساء') {
                            $scope.selectedTiming = 'EVENING';
                            }
                        }
            $scope.getDelayHotspotsData.setType = $scope.selectedTiming;
            //ComparisionSet
            $http({
            async: true,
                crossDomain: true,
                    method: 'POST',
                    url: basePath + 'Traffic/getDelayHotspotsByYearQuarterAndType',
                    headers: {
                'Content-Type': 'application/json'
                },
                    data: $scope.getDelayHotspotsData
                    }).then(function (data) {

            $scope.comparisionSet = data.data;
            $scope.comparisionSetData1 = {
                };
                $scope.baseSetData1 = {
                };
                $scope.showSpinner = true;
                if ($scope.comparisionSet.jsonURL === "not exist") {
                    if ($scope.selecteLanguage == 'en') {
                        alert($scope.sideNav[56].navEnglish);
                        } else {
                        alert($scope.sideNav[56].navArabic);
                        }

                    }
                else {
                    $http({
                        crossDomain: true,
                            method: 'GET',
                                url: $scope.comparisionSet.jsonURL,
                            headers: {
                            'Content-Type': 'application/json'
                            }
                            }).then(function (result) {

                        $scope.comparisionSetData = result.data;
                        var length = $scope.comparisionSetData.network.segmentResults.length;
                        var averageSpeed = 0;
                        var harmonicAverageSpeed = 0;
                        var streetName = ' ';
                        var propertyName = 'differenceTime';
                        var diffTime = 0;
                        var delayHotspotsLines =[];

                        if ($scope.baseSetData == null || $scope.baseSetData == undefined) {
                            $timeout(function () {
                                $scope.getBasesetByYearQuarter($scope.selectedQuarter, $scope.selectedYear);
                                }, 5000);

                                } else {
                            if ($scope.baseSetData.network.segmentResults.length === $scope.comparisionSetData.network.segmentResults.length) {

                                for (var i = 0; i < length; i++) {
                                    if ($scope.baseSetData.network.segmentResults[i].segmentId === $scope.comparisionSetData.network.segmentResults[i].segmentId) {
                                        diffTime = $scope.comparisionSetData.network.segmentResults[i].segmentTimeResults[0].averageTravelTime -$scope.baseSetData.network.segmentResults[i].segmentTimeResults[0].averageTravelTime;
                                    }
                                    $scope.comparisionSetData.network.segmentResults[i].differenceTime = Math.round(diffTime);
                                    $scope.baseSetData.network.segmentResults[i].differenceTime = Math.round(diffTime);
                                }


                                $scope.comparisionSetData1.network = { 
                                };
                                $scope.comparisionSetData1.network.segmentResults =[];



                                $scope.comparisionSetData1.network.segmentResults = orderBy($scope.comparisionSetData.network.segmentResults, propertyName, true);

                                $scope.baseSetData1.network = {
                                };
                                $scope.baseSetData1.network.segmentResults =[];
                                $scope.baseSetData1.network.segmentResults = orderBy($scope.baseSetData.network.segmentResults, propertyName, true);
                                clearMarkers();

                                for (var i = 0; i < 10; i++) {
                                    var freeFlowSpeed = Math.round($scope.baseSetData1.network.segmentResults[i].segmentTimeResults[0].averageSpeed);
                                    if (!$scope.comparisionSetData1.network.segmentResults[i].streetName || $scope.comparisionSetData1.network.segmentResults[i].distance < 500) {
                                        $scope.comparisionSetData1.network.segmentResults.splice(i, 1);
                                        $scope.baseSetData1.network.segmentResults.splice(i, 1);
                                        i = i -1;
                                    } else {

                                        var LatLng = {
                                                lat: $scope.comparisionSetData1.network.segmentResults[i].shape[0].latitude, lng: $scope.comparisionSetData1.network.segmentResults[i].shape[0].longitude
                                            };
                                        $scope.comparisionSetData1.network.segmentResults[i].LatLng = {
                                                lat: LatLng.lat, lng: LatLng.lng
                                            };
                                        var contentString = ``;
                                        // $scope.comparisionSetData1.network.segmentResults[i].delayScore = Math.random(100);
                                        $scope.comparisionSetData1.network.segmentResults[i].distance = Math.round($scope.comparisionSetData1.network.segmentResults[i].distance);
                                        $scope.comparisionSetData1.network.segmentResults[i].img = 'images/markerImg.png';
                                        $scope.comparisionSetData1.network.segmentResults[i].img1 = 'images/markerImg (' +(i +1) + ').png';

                                        if (i <= 8) {
                                            if ($scope.selecteLanguage == 'en') {
                                                contentString = `<div class="jam1"><img src="./${$scope.comparisionSetData1.network.segmentResults[i].img}"  alt="Alternate Text" /><p class="map-icons1">${i +1}</p><h6><span><STRONG>${$scope.comparisionSetData1.network.segmentResults[i].streetName.toUpperCase() }</STRONG></span></h6></div><h6><span>${$scope.sideNav[38].navEnglish} : ${Math.round($scope.comparisionSetData1.network.segmentResults[i].distance) } mtrs</span></h6><h6><span>${$scope.sideNav[39].navEnglish} : ${Math.round($scope.comparisionSetData1.network.segmentResults[i].differenceTime) }sec</span></h6><h6><span>${$scope.sideNav[40].navEnglish} : ${Math.round($scope.comparisionSetData1.network.segmentResults[i].segmentTimeResults[0].averageSpeed) }km/h</span></h6><h6><span>${$scope.sideNav[41].navEnglish} : ${Math.round($scope.baseSetData1.network.segmentResults[i].segmentTimeResults[0].averageSpeed) }km/h</span></h6><h6><span>${$scope.sideNav[42].navEnglish} : ${Math.round($scope.comparisionSetData1.network.segmentResults[i].segmentTimeResults[0].averageTravelTime) }sec</span></h6><h6><span>${$scope.sideNav[44].navEnglish} : ${Math.round($scope.baseSetData1.network.segmentResults[i].segmentTimeResults[0].averageTravelTime) }sec</span></h6>`;//<h6><span>Delay Score : ${$scope.comparisionSetData1.network.segmentResults[i].delayScore}</span></h6>
                                            } else {
                                                contentString = `<div class="jam1"><img src="./${$scope.comparisionSetData1.network.segmentResults[i].img}"  alt="Alternate Text" /><p class="map-icons1">${i +1}</p><h6><span><STRONG>${$scope.comparisionSetData1.network.segmentResults[i].streetName.toUpperCase() }</STRONG></span></h6></div><h6><span>${$scope.sideNav[38].navArabic} : متر ${Math.round($scope.comparisionSetData1.network.segmentResults[i].distance) }</span></h6><h6><span>  ${$scope.sideNav[39].navArabic} : ثواني ${Math.round($scope.comparisionSetData1.network.segmentResults[i].differenceTime) }  </span></h6><h6><span>${$scope.sideNav[40].navArabic} : كيلومتر في الساعة ${Math.round($scope.comparisionSetData1.network.segmentResults[i].segmentTimeResults[0].averageSpeed) }</span></h6><h6><span> ${$scope.sideNav[41].navArabic} : كيلومتر في الساعة ${Math.round($scope.baseSetData1.network.segmentResults[i].segmentTimeResults[0].averageSpeed) }</span></h6><h6><span> ${$scope.sideNav[42].navArabic} : ثواني ${Math.round($scope.comparisionSetData1.network.segmentResults[i].segmentTimeResults[0].averageTravelTime) }  </span></h6><h6><span> ${$scope.sideNav[44].navArabic} : ثواني ${Math.round($scope.baseSetData1.network.segmentResults[i].segmentTimeResults[0].averageTravelTime) } </span></h6>`;//<h6><span>Delay Score : ${$scope.comparisionSetData1.network.segmentResults[i].delayScore}</span></h6>
                                            }

                                            } else {
                                            if ($scope.selecteLanguage == 'en') {
                                                contentString = `<div class="jam1"><img src="./${$scope.comparisionSetData1.network.segmentResults[i].img}"  alt="Alternate Text" /><p class="map-icons2">${i +1}</p><h6><span><STRONG>${$scope.comparisionSetData1.network.segmentResults[i].streetName.toUpperCase() }</STRONG></span></h6></div><h6><span>${$scope.sideNav[38].navEnglish} : ${Math.round($scope.comparisionSetData1.network.segmentResults[i].distance) } mtrs</span></h6><h6><span>${$scope.sideNav[39].navEnglish} : ${Math.round($scope.comparisionSetData1.network.segmentResults[i].differenceTime) }sec</span></h6><h6><span>${$scope.sideNav[40].navEnglish} : ${Math.round($scope.comparisionSetData1.network.segmentResults[i].segmentTimeResults[0].averageSpeed) } km/h</span></h6><h6><span>${$scope.sideNav[41].navEnglish} : ${Math.round($scope.baseSetData1.network.segmentResults[i].segmentTimeResults[0].averageSpeed) }km/h</span></h6><h6><span>${$scope.sideNav[42].navEnglish} : ${Math.round($scope.comparisionSetData1.network.segmentResults[i].segmentTimeResults[0].averageTravelTime) }sec</span></h6><h6><span>${$scope.sideNav[44].navEnglish} : ${Math.round($scope.baseSetData1.network.segmentResults[i].segmentTimeResults[0].averageTravelTime) }sec</span></h6>`;//<h6><span>Delay Score : ${$scope.comparisionSetData1.network.segmentResults[i].delayScore}</span></h6>
                                            } else {
                                                contentString = `<div class="jam1"><img src="./${$scope.comparisionSetData1.network.segmentResults[i].img}"  alt="Alternate Text" /><p class="map-icons2">${i +1}</p><h6><span><STRONG>${$scope.comparisionSetData1.network.segmentResults[i].streetName.toUpperCase() }</STRONG></span></h6></div><h6><span>${$scope.sideNav[38].navArabic} : متر ${Math.round($scope.comparisionSetData1.network.segmentResults[i].distance) }</span></h6><h6><span>  ${$scope.sideNav[39].navArabic} : ثواني ${Math.round($scope.comparisionSetData1.network.segmentResults[i].differenceTime) }  </span></h6><h6><span>${$scope.sideNav[40].navArabic} : كيلومتر في الساعة ${Math.round($scope.comparisionSetData1.network.segmentResults[i].segmentTimeResults[0].averageSpeed) }</span></h6><h6><span> ${$scope.sideNav[41].navArabic} : كيلومتر في الساعة ${Math.round($scope.baseSetData1.network.segmentResults[i].segmentTimeResults[0].averageSpeed) }</span></h6><h6><span> ${$scope.sideNav[42].navArabic} : ثواني ${Math.round($scope.comparisionSetData1.network.segmentResults[i].segmentTimeResults[0].averageTravelTime) }  </span></h6><h6><span> ${$scope.sideNav[44].navArabic} : ثواني ${Math.round($scope.baseSetData1.network.segmentResults[i].segmentTimeResults[0].averageTravelTime) } </span></h6>`;//<h6><span>Delay Score : ${$scope.comparisionSetData1.network.segmentResults[i].delayScore}</span></h6>
                                            }
                                            }

                                        $scope.comparisionSetData1.network.segmentResults[i].contentString = contentString

                                        var title = $scope.comparisionSetData1.network.segmentResults[i].streetName;
                                        var icon = $scope.comparisionSetData1.network.segmentResults[i].img1;
                                        var points = '';
                                        $scope.drawMarker(map, LatLng, contentString, title, icon);
                                        if ($scope.comparisionSetData1.network.segmentResults[i].shape.length > 1) {
                                            for (var j = 0; j < $scope.comparisionSetData1.network.segmentResults[i].shape.length; j++) {
                                                if (j < $scope.comparisionSetData1.network.segmentResults[i].shape.length -1) {

                                                    points = points +$scope.comparisionSetData1.network.segmentResults[i].shape[j].longitude + ' ' +$scope.comparisionSetData1.network.segmentResults[i].shape[j].latitude + ',';
                                                } else {
                                                    points = points +$scope.comparisionSetData1.network.segmentResults[i].shape[j].longitude + ' ' +$scope.comparisionSetData1.network.segmentResults[i].shape[j].latitude;
                                                }

                                            }
                                        }

                                        var firstFeature = format.read("LINESTRING(" + points + ")");
                                        var myFirstLineStyle = OpenLayers.Util.applyDefaults(myFirstLineStyle, OpenLayers.Feature.Vector.style['default']);
                                        myFirstLineStyle.strokeColor = "red";
                                        myFirstLineStyle.strokeWidth = 4;
                                        firstFeature.style = myFirstLineStyle;
                                        delayHotspotsLines.push(firstFeature);
                                    }
                                    }

                                //  console.log("retrivedObject : ", JSON.stringify($scope.comparisionSetData1));
                            }
                            }
                        vectorLayer.addFeatures(delayHotspotsLines);
                        map.addLayer(vectorLayer);
                        $scope.showSpinner = false;
                        if ($scope.selecteLanguage == 'ar') {

                            if ($scope.selectedTiming == 'MORNING') {
                                $scope.selectedTiming = 'صباح';
                                } else if ($scope.selectedTiming == 'AFTERNOON') {
                                    $scope.selectedTiming = 'بعد الظهر';
                                    } else if ($scope.selectedTiming == 'EVENING') {
                                        $scope.selectedTiming = 'مساء';
                                        }
                                    }
                        addNano();
                    });



                        }



                }).catch(function (data) {
            //console.log('Cant get data');
            console.log(data);

            });

            }

        function addNano() {
            $timeout(function () {
                $(".nano").nanoScroller();
            }, 1000);
                }
                }
                }) ();
