﻿(function () {
    'use strict';

    angular
        .module('trafficApp')
        .controller('liveTrafficUpdater', liveTrafficUpdater);

    liveTrafficUpdater.$inject = ['$scope', '$http'];

    function liveTrafficUpdater($scope, $http) {
        var basePath = "/";
        $scope.trafficFlowData = {};
        $scope.viewGraph = false;
        $scope.LatLng = {};
        $scope.LatLng.lat = 25.429723;
        $scope.LatLng.lng = 55.4323196;
        $scope.LatLng.zoom = 10;
        $scope.avgCalculator = "low";
        $scope.avgSpeed = 0;
        $scope.travelTime = {};
        $scope.sideNav = [];
        $scope.LatLngIncident = [];
        $scope.liveTrafficData = {};
        $scope.showSpinner = true;
        $scope.showSpinner1 = true;
        $scope.incidentResults = [];
        $scope.liveTrafficData.LatLng = [];
        $scope.totalCount = 0;
        $scope.optimalSpeed = 0;
        $scope.selectedLanguage = true;
        var graphPresent = 0;
        var liveReload = null;
        var fromProjection = new OpenLayers.Projection("EPSG:4326"); // transform from WGS 1984
        var toProjection = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
        $scope.openLR = [];
        var firstTime = true;
        $scope.openLRCity = [];
        $scope.thtcData = {};
        $scope.thtcData.key = 'VGhEeGIwNjA5MTdjVEluZA==';
        $scope.thtcData.gps = [];
        activate();

        function activate() {
            $scope.selectedLanguage = window.localStorage.getItem('language');

            getSideNav();
            getDBCOntent();
            setTimeout(function () {
                //getSideNav();
                getTimeAndDate();
                getTrafficData();
                getTrafficIncidentsData();
                updateDataIntoDB();
                IncidentBulkFeed();
                //DecodeIncidentBulkFeed();
            }, 1000);


        }

        function getSideNav() {

            $http({
                method: 'POST',
                url: basePath + 'Traffic/getSideNav',
                headers: "application/json"
            }).then(function (data) {
                $scope.sideNav = data.data;
                setTimeout(function () {
                    $(".nano").nanoScroller();
                }, 1000);

                window.localStorage.setItem('showList', JSON.stringify($scope.sideNav[36]));
                window.localStorage.setItem('hideList', JSON.stringify($scope.sideNav[37]));

                window.localStorage.setItem('sideNav', JSON.stringify($scope.sideNav));
            }).catch(function (error) {
                console.log(error)
            });;
        }

        function getDBCOntent() {

            $http({
                method: 'POST',
                url: basePath + 'Traffic/contentManagement',
                headers: "application/json"
            }).then(function (data) {
                $scope.Content = data.data;
                window.localStorage.setItem('content', JSON.stringify($scope.Content));
            }).catch(function (error) {
                console.log(error)
            });;
        }



        function getTimeAndDate() {

            var month = new Array();
            month[0] = "Jan";
            month[1] = "Feb";
            month[2] = "Mar";
            month[3] = "Apr";
            month[4] = "May";
            month[5] = "Jun";
            month[6] = "Jul";
            month[7] = "Aug";
            month[8] = "Sep";
            month[9] = "Oct";
            month[10] = "Nov";
            month[11] = "Dec";

            var currentDate = new Date;

            var day = currentDate.getDate();
            var months = month[currentDate.getMonth()];
            var year = currentDate.getFullYear();
            var time = new Date(currentDate.setMinutes(currentDate.getMinutes() - 90)).toLocaleTimeString();

            $scope.currentDate = {};
            $scope.currentDate.day = day;
            $scope.currentDate.month = months;
            $scope.currentDate.year = year;
            $scope.currentDate.time = time;

        }

        function getTrafficData() {

            //GetRecentDataForLast48Hours
            var url = $scope.Content[32].value + $scope.Content[33].value + $scope.Content[34].value;
            $http({
                method: 'GET',
                url: url,
                headers: "application/json"
            }).then(function (data) {
                $scope.liveTrafficDataApi = data.data;
            }).catch(function (error) {
                console.log(error);
            });
            var url1 = $scope.Content[35].value + $scope.Content[36].value + $scope.Content[37].value;
            $http({
                method: 'GET',
                url: url1,
            }).then(function (data) {

                $scope.trafficFlowData = data.data;

                $scope.avgSpeed = $scope.trafficFlowData.averageCurrentSpeedKmph;

                $scope.optimalSpeed = $scope.trafficFlowData.averageFreeFlowSpeedKmph;

                $scope.optmimumSpeedCalc($scope.avgSpeed, $scope.optimalSpeed);

                $scope.showSpinner1 = false;

            }).catch(function (error) {
                console.log(error);
            });
        }

        function IncidentBulkFeed() {
            //get Incident Bulk Feed 
            var url = $scope.Content[38].value + $scope.Content[39].value + $scope.Content[40].value;
            $http({
                method: 'GET',
                url: url,
                headers: "application/json"
            }).then(function (data) {

                $scope.liveTrafficLevelData = data.data;

                $scope.liveTrafficData.liveTrafficLevel = $scope.liveTrafficLevelData.liveCongestionLevel;

                $scope.liveTrafficData.typicalTrafficLevel = $scope.liveTrafficLevelData.typicalCongestionLevel;

                $scope.speedDate = new Date($scope.liveTrafficLevelData.updateTime).toLocaleTimeString();

                liveTrafficLevel();

            }).catch(function (error) {
                console.log(error);
            });

        }

        function liveTrafficLevel() {
            var x = 60;
            var y = 40;
            var ele = document.getElementById('graph4');
            if ($scope.selectedLanguage == 'en') {
                var format1 = $scope.sideNav[58].navEnglish;
                var format2 = $scope.sideNav[59].navEnglish;
            } else {
                var format1 = $scope.sideNav[58].navArabic;
                var format2 = $scope.sideNav[59].navArabic;
            }

            x = $scope.liveTrafficData.liveTrafficLevel;
            y = $scope.liveTrafficData.typicalTrafficLevel;
            if (ele != null) {
                Morris.Donut({
                    element: 'graph4',
                    data: [
                                    {
                                        value: y, label: y + '%', formatted: format1
                                    },
                                    {
                                        value: x, label: x + '%', formatted: format2
                                    },
                    ],
                    formatter: function (x, data) {
                        return data.formatted;
                    }
                });
            } else {
                clearInterval(liveReload);
            }


        }


        function liveTrafficGraph(graphName) {

            if (graphName === "speed") {
                //var dayData = []; //For morris graph

                var timeX = [];

                var length = $scope.liveTrafficDataApi.apis["0"].snapshots.length;
                var optimalSpeed = [];
                var averageSpeed = [];
                var firstValue = true;
                var diffTime = 0;
                if ($scope.selectedLanguage == 'en') {
                    optimalSpeed.push($scope.sideNav[60].navEnglish);
                    averageSpeed.push($scope.sideNav[61].navEnglish);
                } else {
                    optimalSpeed.push($scope.sideNav[60].navArabic);
                    averageSpeed.push($scope.sideNav[61].navArabic);
                }

                for (var i = 0; i < length; i++) {

                    var time = new Date($scope.liveTrafficDataApi.apis["0"].snapshots[i].timestamp);

                    var day = time.toLocaleDateString([], { weekday: 'short' });
                    if (firstValue) {
                        diffTime = time.getMinutes();
                        firstValue = false;
                    }


                    time = time.toLocaleTimeString([], {
                        hour: '2-digit', minute: '2-digit'
                    });

                    if (diffTime <= 30) {
                        if (time.includes(":00")) {
                            var optiSpeed = $scope.optimalSpeed;

                            optimalSpeed.push(optiSpeed);

                            averageSpeed.push($scope.liveTrafficDataApi.apis["0"].snapshots[i].value.averageCurrentSpeedKmph)
                            //dayData.push(data);  //For morris graph

                            timeX.push(day + " " + time);
                        }
                    } else if (diffTime > 30) {
                        if (time.includes(":30")) {
                            var optiSpeed = 42;

                            optimalSpeed.push(optiSpeed);

                            averageSpeed.push($scope.liveTrafficDataApi.apis["0"].snapshots[i].value.averageCurrentSpeedKmph)
                            //dayData.push(data);  //For morris graph

                            timeX.push(day + " " + time);
                        }
                    }



                }

                c3.chart.internal.fn.categoryName = function (i) {
                    var config = this.config, categoryIndex = Math.ceil(i);
                    return i < config.axis_x_categories.length ? config.axis_x_categories[categoryIndex] : i;
                };
                var chart = c3.generate({
                    bindto: '#chart',
                    point: {
                        r: 4
                    },
                    data: {
                        columns: [
                    optimalSpeed,
                    averageSpeed
                        ],
                        type: 'spline'
                    },

                    axis: {
                        y: {
                            tick: {
                                format: function (d) {
                                    return d + "Km/h";
                                }
                            }
                        },
                        x: {
                            type: 'category',
                            categories: timeX,
                            tick: {
                                centered: true,
                                count: 10,
                                rotate: -35,
                                multiline: true
                            },

                            height: 100

                        }
                    },
                    legend: {
                        show: false
                    }
                });

            } else if (graphName === "reports") {

                var timeX = [];
                var firstValue = true;
                var diffTime = 0;
                var length = $scope.liveTrafficDataApi.apis["2"].snapshots.length;
                var roadWorks = [];
                var jams = [];
                var closures = [];
                var total = [];
                if ($scope.selectedLanguage == 'en') {
                    roadWorks.push($scope.sideNav[62].navEnglish);
                    jams.push($scope.sideNav[63].navEnglish);
                    closures.push($scope.sideNav[64].navEnglish);
                    total.push($scope.sideNav[65].navEnglish);
                }
                else {
                    roadWorks.push($scope.sideNav[62].navArabic);
                    jams.push($scope.sideNav[63].navArabic);
                    closures.push($scope.sideNav[64].navArabic);
                    total.push($scope.sideNav[65].navArabic);
                }

                for (var i = 0; i < length; i++) {
                    var time = new Date($scope.liveTrafficDataApi.apis["2"].snapshots[i].timestamp);

                    var day = time.toLocaleDateString([], { weekday: 'short' });

                    if (firstValue) {
                        diffTime = time.getMinutes();
                        firstValue = false;
                    }

                    time = time.toLocaleTimeString([], {
                        hour: '2-digit', minute: '2-digit'
                    });


                    if (diffTime <= 30) {
                        if (time.includes(":00")) {
                            roadWorks.push($scope.liveTrafficDataApi.apis["2"].snapshots[i].value.roadworks.count);


                            jams.push($scope.liveTrafficDataApi.apis["2"].snapshots[i].value.jams.count);

                            closures.push($scope.liveTrafficDataApi.apis["2"].snapshots[i].value.closures.count);

                            $scope.liveTrafficDataApi.apis["2"].snapshots[i].total = Number($scope.liveTrafficDataApi.apis["2"].snapshots[i].value.roadworks.count) + Number($scope.liveTrafficDataApi.apis["2"].snapshots[i].value.jams.count) + Number($scope.liveTrafficDataApi.apis["2"].snapshots[i].value.closures.count);


                            total.push($scope.liveTrafficDataApi.apis["2"].snapshots[i].total);

                            timeX.push(day + " " + time);
                        }


                    } else if (diffTime > 30) {
                        if (time.includes(":30")) {
                            roadWorks.push($scope.liveTrafficDataApi.apis["2"].snapshots[i].value.roadworks.count);


                            jams.push($scope.liveTrafficDataApi.apis["2"].snapshots[i].value.jams.count);

                            closures.push($scope.liveTrafficDataApi.apis["2"].snapshots[i].value.closures.count);

                            $scope.liveTrafficDataApi.apis["2"].snapshots[i].total = Number($scope.liveTrafficDataApi.apis["2"].snapshots[i].value.roadworks.count) + Number($scope.liveTrafficDataApi.apis["2"].snapshots[i].value.jams.count) + Number($scope.liveTrafficDataApi.apis["2"].snapshots[i].value.closures.count);


                            total.push($scope.liveTrafficDataApi.apis["2"].snapshots[i].total);

                            timeX.push(day + " " + time);
                        }
                    }

                }

                c3.chart.internal.fn.categoryName = function (i) {
                    var config = this.config, categoryIndex = Math.ceil(i);
                    return i < config.axis_x_categories.length ? config.axis_x_categories[categoryIndex] : i;
                };
                if ($scope.selectedLanguage == 'en') {
                    var colors = {
                        "Road Works": '#4d4948',
                        Jams: '#017cc2',
                        Closures: '#85c226',
                        Total: 'red'

                    }
                } else {
                    var colors = {
                        "أعمال الطرق": '#4d4948',
                        مربيات: '#017cc2',
                        إغلاق: '#85c226',
                        مجموع: 'red'

                    }
                }

                var chart = c3.generate({
                    bindto: '#chart',
                    point: {
                        r: 4
                    },
                    data: {
                        columns: [
                    roadWorks,
                    jams,
                    closures,
                    total
                        ],
                        type: 'spline',
                        colors: colors
                    },

                    axis: {
                        y: {
                            tick: {
                                format: function (d) {
                                    return d; //+ "Km/h";
                                }
                            }
                        },
                        x: {
                            type: 'category',
                            categories: timeX,
                            tick: {
                                centered: true,
                                count: 10,
                                rotate: -35,
                                multiline: false
                            },

                            height: 100

                        }
                    },
                    legend: {
                        show: false
                    }
                });



            } else if (graphName === "delays") {

                var timeX = [];
                var firstValue = true;
                var diffTime = 0;
                var length = $scope.liveTrafficDataApi.apis["2"].snapshots.length;
                var delay = [];
                //var hoursMin = [];
                if ($scope.selectedLanguage == 'en') {
                    delay.push($scope.sideNav[66].navEnglish);
                } else {
                    delay.push($scope.sideNav[66].navArabic);
                }

                //hoursMin.push('Mins');
                for (var i = 0; i < length; i++) {

                    var time = new Date($scope.liveTrafficDataApi.apis["2"].snapshots[i].timestamp);

                    var day = time.toLocaleDateString([], { weekday: 'short' });
                    if (firstValue) {
                        diffTime = time.getMinutes();
                        firstValue = false;
                    }
                    time = time.toLocaleTimeString([], {
                        hour: '2-digit', minute: '2-digit'
                    });

                    if (diffTime <= 30) {
                        if (time.includes(":00")) {
                            delay.push(Number($scope.liveTrafficDataApi.apis["2"].snapshots[i].value.jams.delay));

                            timeX.push(day + " " + time);
                        }
                    } else if (diffTime > 30) {
                        if (time.includes(":30")) {
                            delay.push(Number($scope.liveTrafficDataApi.apis["2"].snapshots[i].value.jams.delay));

                            timeX.push(day + " " + time);
                        }
                    }


                }

                c3.chart.internal.fn.categoryName = function (i) {
                    var config = this.config, categoryIndex = Math.ceil(i);
                    return i < config.axis_x_categories.length ? config.axis_x_categories[categoryIndex] : i;
                };
                var chart = c3.generate({
                    bindto: '#chart',
                    point: {
                        r: 4
                    },
                    data: {
                        columns: [
                    delay

                        ],
                        type: 'spline'

                    },

                    axis: {
                        y: {
                            tick: {
                                format: function (d) {
                                    var x = {};
                                    x.hrs = Math.floor(d / 60);
                                    x.mins = Math.floor(d % 60);

                                    return x.hrs + "Hrs : " + x.mins + "Mins";
                                }
                            }
                        },
                        x: {
                            type: 'category',
                            categories: timeX,
                            tick: {
                                centered: true,
                                count: 10,
                                rotate: -35,
                                multiline: false
                            },

                            height: 100

                        }
                    },
                    tooltip: {
                        sorted: false
                    },
                    legend: {
                        show: false
                    }
                });



            } else if (graphName === "levels") {

                var timeX = [];
                var firstValue = true;
                var diffTime = 0;
                var length = $scope.liveTrafficDataApi.apis["1"].snapshots.length;
                var typicalLevel = [];
                var nowLevel = [];
                if ($scope.selectedLanguage == 'en') {

                    typicalLevel.push($scope.sideNav[67].navEnglish);
                    nowLevel.push($scope.sideNav[68].navEnglish);
                } else {
                    typicalLevel.push($scope.sideNav[67].navArabic);
                    nowLevel.push($scope.sideNav[68].navArabic);
                }


                for (var i = 0; i < length; i++) {

                    var time = new Date($scope.liveTrafficDataApi.apis["1"].snapshots[i].timestamp);

                    var day = time.toLocaleDateString([], { weekday: 'short' });
                    if (firstValue) {
                        diffTime = time.getMinutes();
                        firstValue = false;
                    }
                    time = time.toLocaleTimeString([], {
                        hour: '2-digit', minute: '2-digit'
                    });
                    if (diffTime <= 30) {
                        if (time.includes(":00")) {
                            typicalLevel.push($scope.liveTrafficDataApi.apis["1"].snapshots[i].value.typicalCongestionLevel);
                            nowLevel.push($scope.liveTrafficDataApi.apis["1"].snapshots[i].value.liveCongestionLevel);

                            timeX.push(day + " " + time);

                        }
                    } else if (diffTime > 30) {
                        if (time.includes(":30")) {
                            typicalLevel.push($scope.liveTrafficDataApi.apis["1"].snapshots[i].value.typicalCongestionLevel);
                            nowLevel.push($scope.liveTrafficDataApi.apis["1"].snapshots[i].value.liveCongestionLevel);

                            timeX.push(day + " " + time);
                        }
                    }
                }

                c3.chart.internal.fn.categoryName = function (i) {
                    var config = this.config, categoryIndex = Math.ceil(i);
                    return i < config.axis_x_categories.length ? config.axis_x_categories[categoryIndex] : i;
                };
                if ($scope.selectedLanguage == 'en') {
                    var colors = {
                        "Typical Traffic Level": '#76c043',
                        "Traffic Level": '#fcb216'
                    };
                } else {
                    var colors = {
                        "مستوى حركة المرور النموذجي": '#76c043',
                        "مستوى حركة المرور": '#fcb216'
                    };
                }

                var chart = c3.generate({
                    bindto: '#chart',
                    point: {
                        r: 4
                    },
                    data: {
                        columns: [
                    typicalLevel,
                    nowLevel
                        ],
                        type: 'spline',
                        colors: colors
                    },

                    axis: {
                        y: {
                            tick: {
                                format: function (d) {
                                    return d;
                                }
                            }
                        },
                        x: {
                            type: 'category',
                            categories: timeX,
                            tick: {
                                centered: true,
                                count: 10,
                                rotate: -35,
                                multiline: false
                            },

                            height: 100

                        }
                    },
                    tooltip: {
                        sorted: false
                    },
                    legend: {
                        show: false
                    }
                });



            }

        }

        $scope.optmimumSpeedCalc = function (currentSpeed, optimalSpeed) {
            var avgCalculator = (100 * currentSpeed) / optimalSpeed;

            if (avgCalculator <= 100) {
                $scope.trafficFlowData.optimalSpeedPercentile = Math.ceil(100 - avgCalculator);
                $scope.avgCalculator = "low";
            } else {
                $scope.trafficFlowData.optimalSpeedPercentile = Math.ceil(avgCalculator - 100);
                $scope.avgCalculator = "high";
            }


        }


        $scope.showGraph = function (graphName) {

            if ($scope.graphName == graphName) {
                if ($scope.viewGraph === true) {

                    $scope.viewGraph = false;
                }
                else {
                    setTimeout(function () {
                        liveTrafficGraph(graphName);
                    }, 1000);
                    $scope.viewGraph = true;
                }

            } else {
                setTimeout(function () {
                    liveTrafficGraph(graphName);
                }, 1000);
                $scope.graphName = graphName;
                $scope.viewGraph = true;
            }


        }

        $scope.closeGraph = function () {
            $scope.viewGraph = false;
        }

        function getTrafficIncidentsData() {
            var url = $scope.Content[41].value + $scope.Content[42].value + $scope.Content[43].value;
            $http({
                method: 'GET',
                url: url,
            }).then(function (data) {
                $scope.incidentResults = data.data;
                var currentDate = new Date();

                var incidentDate = new Date(data.data.updateTime);
                var offset = currentDate.getTimezoneOffset();
                //$scope.incidentDate = new Date(incidentDate.setTime(incidentDate.getTime() - (offset * 60 * 1000))).toLocaleTimeString();
                $scope.incidentDate = new Date(incidentDate.setTime(incidentDate.getTime())).toLocaleTimeString();
                window.localStorage.setItem("updatedTime", $scope.incidentDate);

                var totalTime = $scope.incidentResults.jams.delay;

                $scope.travelTime.hour = Math.floor(totalTime / 60);
                $scope.travelTime.minutes = Math.round(totalTime % 60);

                $scope.showSpinner = false;

                $scope.jamCount = $scope.incidentResults.jams.count;
                $scope.workCount = $scope.incidentResults.roadworks.count;
                $scope.closedCount = $scope.incidentResults.closures.count;

                $scope.totalCount = $scope.jamCount + $scope.workCount + $scope.closedCount;

                var jamCount = 0;
                if ($scope.jamCount > 100) {
                    jamCount = 100;
                } else {
                    jamCount = $scope.jamCount;
                }

                setTimeout(function () {
                    $('.speed-progress1 .process p span').css('width', $scope.workCount + '%');
                    $('.speed-progress2 .process p span').css('width', jamCount + '%');
                    $('.speed-progress3 .process p span').css('width', $scope.closedCount + '%');
                });

            }).catch(function (error) {
                console.log(error)
            });



        }

        function updateDataIntoDB() {
            $http({
                method: 'POST',
                url: basePath + 'Traffic/getTrafficData',
                headers: "application/json"
            }).then(function (data) {
                $scope.liveTrafficDataDB = data.data;

                for (var i = 0; i < $scope.liveTrafficDataDB.length; i++) {
                    $scope.liveTrafficDataDB[i].time = new Date($scope.liveTrafficDataDB[i].time).toLocaleString();
                }

                StoreDataIntoDB();
            }).catch(function (error) {
                console.log(error)
            });

            setInterval(function () {
                $http({
                    method: 'POST',
                    url: basePath + 'Traffic/getTrafficData',
                    headers: "application/json"
                }).then(function (data) {
                    $scope.liveTrafficDataDB = data.data;
                    for (var i = 0; i < $scope.liveTrafficDataDB.length; i++) {
                        $scope.liveTrafficDataDB[i].time = new Date($scope.liveTrafficDataDB[i].time).toLocaleString();
                    }
                    StoreDataIntoDB();
                    IncidentBulkFeed();
                }).catch(function (error) {
                    console.log(error)
                });
            }, (1 / 2) * 60 * 1000)

        }

        function StoreDataIntoDB() {

            var currentDate = new Date();
            if ($scope.liveTrafficDataDB.length !== 0) {
                var dbDate = new Date($scope.liveTrafficDataDB["47"].time);
                var timeDiff = Math.abs(currentDate.getTime() - dbDate.getTime());

                var diffTime = Math.ceil(timeDiff / 60000);
                if (diffTime >= 30 || $scope.liveTrafficDataDB["47"].time === null) {

                    if ($scope.avgSpeed !== 0) {
                        if ($scope.avgSpeed >= 41) {
                            $scope.liveTrafficData.liveTrafficSpeed = 41;
                        } else {
                            $scope.liveTrafficData.liveTrafficSpeed = $scope.avgSpeed;
                        }
                        $scope.liveTrafficData.roadWorks = $scope.workCount;
                        $scope.liveTrafficData.jams = $scope.jamCount;
                        $scope.liveTrafficData.closures = $scope.closedCount;
                        $scope.liveTrafficData.liveTrafficDelay = $scope.travelTime.hour + ', ' + $scope.travelTime.minutes;

                        $http({
                            method: 'POST',
                            data: $scope.liveTrafficData,
                            url: basePath + 'Traffic/storeTrafficData',
                            headers: "application/json"
                        }).then(function (data) {
                            $scope.storeTraffic = data.data;

                        }).catch(function (error) {
                            console.log(error)
                        });;
                    }
                }
                else {
                    if ($scope.liveTrafficDataDB.length !== 0) {
                        if ($scope.liveTrafficDataDB["47"].liveTrafficSpeed !== 0) {
                            if ($scope.avgSpeed != $scope.liveTrafficDataDB["47"].liveTrafficSpeed && $scope.avgSpeed !== 0) {
                                $scope.avgSpeed = $scope.avgSpeed;
                                $scope.optmimumSpeedCalc($scope.avgSpeed, $scope.optimalSpeed);
                                getTrafficData();
                                getTrafficIncidentsData();
                            } else {
                                $scope.avgSpeed = Math.round($scope.liveTrafficDataDB["47"].liveTrafficSpeed);
                                $scope.optmimumSpeedCalc($scope.avgSpeed, $scope.optimalSpeed);
                                getTrafficIncidentsData();
                                getTrafficData();
                            }

                        }

                    }

                }
            } else {

                // For First Entry in DB
                $scope.liveTrafficData.liveTrafficSpeed = $scope.avgSpeed;
                $scope.liveTrafficData.roadWorks = $scope.workCount;
                $scope.liveTrafficData.jams = $scope.jamCount;
                $scope.liveTrafficData.closures = $scope.closedCount;
                $scope.liveTrafficData.liveTrafficDelay = $scope.travelTime.hour + ', ' + $scope.travelTime.minutes;
                if ($scope.avgSpeed !== 0) {
                    $http({
                        method: 'POST',
                        data: $scope.liveTrafficData,
                        url: basePath + 'Traffic/storeTrafficData',
                        headers: "application/json"
                    }).then(function (data) {
                        $scope.storeTraffic = data.data;

                    }).catch(function (error) {
                        console.log(error)
                    });;
                }
            }

        }

        function secondsToMinsHrs(d) {
            //d = Number(d);
            var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            var s = Math.floor(d % 3600 % 60);

            return {
                hour: h, minutes: m, seconds: s
            };
        }
    }
})();
