﻿(function () {
    'use strict';

    angular
        .module('trafficApp')
        .controller('trafficIncidents', trafficIncidents);

    trafficIncidents.$inject = ['$scope', '$http', '$rootScope', 'orderByFilter'];

    function trafficIncidents($scope, $http, $rootScope, orderBy) {
        $scope.title = 'trafficIncidents';
        $scope.selectedTab = 'trafficIncidents';

        $scope.incidentResults = [];
        $scope.activated = true;
        $scope.showSpinner = false;
        $scope.sideNav = [];
        var basePath = '/';
        var map;
        //var markers = [];
        activate();
        var fromProjection = new OpenLayers.Projection("EPSG:4326"); // transform from WGS 1984
        var toProjection = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection

        function activate() {
            $scope.selectedLanguage = window.localStorage.getItem('language');
            $scope.Content = JSON.parse(window.localStorage.getItem('content'));
            $scope.sideNav = JSON.parse(window.localStorage.getItem('sideNav'));
            //getSideNav();
            //getDBCOntent();
            setTimeout(function () {
                if ($scope.selectedLanguage == null || $scope.selectedLanguage == undefined) {
                    window.location.reload();
                }
                initMap();
                getTimeAndDate();
               
            }, 1000);

            setInterval(function () {
                $scope.incidentDate = window.localStorage.getItem('updatedTime');
                getIncidentsData();
            }, 3 * 60 * 1000);

        }


        function getTimeAndDate() {
            var month = new Array();
            month[0] = "Jan";
            month[1] = "Feb";
            month[2] = "Mar";
            month[3] = "Apr";
            month[4] = "May";
            month[5] = "Jun";
            month[6] = "Jul";
            month[7] = "Aug";
            month[8] = "Sep";
            month[9] = "Oct";
            month[10] = "Nov";
            month[11] = "Dec";

            var currentDate = new Date;

            var day = currentDate.getDate();
            var months = month[currentDate.getMonth()];
            var year = currentDate.getFullYear();
            var time = new Date(currentDate.setMinutes(currentDate.getMinutes() - 90)).toLocaleTimeString();

            $scope.currentDate = {};
            $scope.currentDate.day = day;
            $scope.currentDate.month = months;
            $scope.currentDate.year = year;
            $scope.currentDate.time = time;

        }

        function initMap() {
            // create the maps
            var extent = new OpenLayers.Bounds(Number($scope.Content[0].value), Number($scope.Content[1].value), Number($scope.Content[2].value), Number($scope.Content[3].value)).transform(fromProjection, toProjection);
            var offset = Number($scope.Content[4].value); // control max zoom-out level

            var options = {
                resolution: Number($scope.Content[5].value),
                numZoomLevels: Number($scope.Content[6].value) - offset, // control zoom-in level
                maxResolution: Number($scope.Content[7].value) / Math.pow(2, offset),
                maxExtent: new OpenLayers.Bounds(Number($scope.Content[8].value), Number($scope.Content[9].value), Number($scope.Content[10].value), Number($scope.Content[11].value)),
                projection: "EPSG:900913",
                units: 'm',
                format: $scope.Content[15].value,
                restrictedExtent: extent,
                eventListeners: {
                    "moveend": getIncidentsData,
                    "click": clearPopup
                }
            };

            map = new OpenLayers.Map("map2", options);

            if ($scope.selectedLanguage == 'en') {
                var layers = $scope.Content[13].value;
            } else {
                var layers = $scope.Content[48].value;
            }

            var wms_th = new OpenLayers.Layer.WMS(
                 $scope.Content[12].value,
                 $scope.Content[25].value,
                   {
                       sphericalMercator: true,
                       layers: layers,
                       tiled: $scope.Content[14].value,
                       format: $scope.Content[15].value,
                       tilesOrigin: map.maxExtent.left + ',' + map.maxExtent.bottom
                   },
                   {
                       buffer: 0,
                       isBaseLayer: true,

                       displayOutsideMaxExtent: true
                   }
                  );

            var flowUrl = $scope.Content[26].value;

            var incidentUrl = $scope.Content[27].value;

            var TTFlow = new OpenLayers.Layer.XYZ("Flow Layer", flowUrl, { zoomOffset: offset, isBaseLayer: false });

            var TTIncident = new OpenLayers.Layer.XYZ("Incident Layer", incidentUrl, { zoomOffset: offset, isBaseLayer: false });

            var markers = new OpenLayers.Layer.Markers("Markers", { zoomOffset: offset, isBaseLayer: false });

            var labelUrl = $scope.Content[17].value;

            var nameLayerTTFlow = new OpenLayers.Layer.XYZ("Label Layer", labelUrl, { zoomOffset: offset, isBaseLayer: false });

            if ($scope.selectedLanguage == 'en') {
                map.addLayers([TTFlow, TTIncident, wms_th, nameLayerTTFlow, markers]);
            } else {
                map.addLayers([TTFlow, TTIncident, wms_th, markers]);
            }


            map.zoomToMaxExtent();
            map.setCenter(
               new OpenLayers.LonLat($scope.Content[18].value, $scope.Content[19].value).transform(fromProjection, toProjection),
               Number($scope.Content[49].value)
           );

            $scope.map = map;

            //debugger
            //var SW = new OpenLayers.LonLat(54.852, 24.449).transform(fromProjection, toProjection);
            //var NE = new OpenLayers.LonLat(55.679, 25.620).transform(fromProjection, toProjection);

        }

        var getData = true;

        function getIncidentsData() {

            if (!getData) {
                var bounds = map.getExtent();
                var zoom = map.getZoom() + Number($scope.Content[4].value);
                var northEast = { lat: bounds.right, lng: bounds.top, zoom: zoom }
                var southWest = { lat: bounds.left, lng: bounds.bottom, zoom: zoom }

                var listData = [];


                listData.push(southWest);
                listData.push(northEast);
                $scope.showSpinner = true;
                getData = false;

                $http({
                    method: 'POST',
                    url: basePath + 'Traffic/TrafficIncidents',
                    data: listData,
                    headers: "application/json"
                }).then(function (data) {
                    clearMarkers();
                    hidePopups();
                    $scope.incidentResults = data.data;
                    var counter = 0;
                    listData = [];

                    var currentDate = new Date();
                    var incidentDate = new Date(data.data.updatedTime);
                    var offset = currentDate.getTimezoneOffset();


                    $scope.showSpinner = false;
                    $scope.propertyName = 'dl';

                    if ($scope.incidentResults.tm.poi.length != null || $scope.incidentResults.tm.poi.length != undefined) {



                        for (var i = 0; i < $scope.incidentResults.tm.poi.length; i++) {

                            if ($scope.incidentResults.tm.poi[i].d != null) {

                            }
                            else {
                                var length = $scope.incidentResults.tm.poi[i].cpoi.length;
                                for (var j = 0; j < length; j++) {
                                    $scope.incidentResults.tm.poi.push($scope.incidentResults.tm.poi[i].cpoi[j]);
                                }
                                $scope.incidentResults.tm.poi.splice(i, 1);
                                i = i - 1;
                            }
                        }

                        for (var i = 0; i < $scope.incidentResults.tm.poi.length; i++) {

                            if ($scope.incidentResults.tm.poi[i].d != null) {
                                var LatLng = { lat: $scope.incidentResults.tm.poi[i].p.y, lng: $scope.incidentResults.tm.poi[i].p.x };
                                $scope.incidentResults.tm.poi[i].LatLng = { lat: $scope.incidentResults.tm.poi[i].p.y, lng: $scope.incidentResults.tm.poi[i].p.x };
                                var contentString = ``;
                                if ($scope.incidentResults.tm.poi[i].d == 'closed') {//مغلق
                                    $scope.incidentResults.tm.poi[i].img = 'images/closed.png';
                                    if ($scope.selectedLanguage == 'en') {
                                        contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>ROAD CLOSED</STRONG></span></h6></div><h6><span>${$scope.incidentResults.tm.poi[i].d}</span></h6><h6><span>Length : ${$scope.incidentResults.tm.poi[i].l} mtrs</span></h6>`;
                                        $scope.incidentResults.tm.poi[i].contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>ROAD CLOSED</STRONG></span></h6></div><h6><span>${$scope.incidentResults.tm.poi[i].d}</span></h6><h6><span>Length : ${$scope.incidentResults.tm.poi[i].l} mtrs</span></h6>`;
                                    } else {
                                        $scope.incidentResults.tm.poi[i].dar = 'مغلق';
                                        contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>الطريق مغلق</STRONG></span></h6></div><h6><span>مغلق</span></h6><h6><span> ${$scope.incidentResults.tm.poi[i].l}  الطول : متر</span></h6>`;
                                        $scope.incidentResults.tm.poi[i].contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>الطريق مغلق</STRONG></span></h6></div><h6><span>مغلق</span></h6><h6><span> ${$scope.incidentResults.tm.poi[i].l}  الطول : متر</span></h6>`;
                                    }


                                } else if ($scope.incidentResults.tm.poi[i].d == 'queuing traffic') {//انتظار مروري
                                    $scope.incidentResults.tm.poi[i].img = 'images/car-org.png';

                                    if ($scope.selectedLanguage == 'en') {
                                        contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>JAM</STRONG></span></h6></div><h6><span>${$scope.incidentResults.tm.poi[i].d}</span></h6><h6><span>Length : ${$scope.incidentResults.tm.poi[i].l} mtrs</span></h6><h6><span>Delay : ${$scope.incidentResults.tm.poi[i].dl} sec</span></h6>`;
                                        $scope.incidentResults.tm.poi[i].contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>JAM</STRONG></span></h6></div><h6><span>${$scope.incidentResults.tm.poi[i].d}</span></h6><h6><span>Length : ${$scope.incidentResults.tm.poi[i].l} mtrs</span></h6><h6><span>Delay : ${$scope.incidentResults.tm.poi[i].dl} sec</span></h6>`;
                                    } else {
                                        $scope.incidentResults.tm.poi[i].dar = 'انتظار مروري';
                                        contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>مربى</STRONG></span></h6></div><h6><span>${$scope.incidentResults.tm.poi[i].dar}</span></h6><h6><span>${$scope.incidentResults.tm.poi[i].l} الطول : متر</span></h6><h6><span>${$scope.incidentResults.tm.poi[i].dl} التأخير: ثانية</span></h6>`;
                                        $scope.incidentResults.tm.poi[i].contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>مربى</STRONG></span></h6></div><h6><span>${$scope.incidentResults.tm.poi[i].dar}</span></h6><h6><span>${$scope.incidentResults.tm.poi[i].l} الطول : متر</span></h6><h6><span>${$scope.incidentResults.tm.poi[i].dl} التأخير: ثانية</span></h6>`;;
                                    }


                                } else if ($scope.incidentResults.tm.poi[i].d == 'stationary traffic') {//توقف مروري
                                    $scope.incidentResults.tm.poi[i].img = 'images/car-red.png';

                                    if ($scope.selectedLanguage == 'en') {
                                        contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>JAM</STRONG></span></h6></div><h6><span>${$scope.incidentResults.tm.poi[i].d}</span></h6><h6><span>Length : ${$scope.incidentResults.tm.poi[i].l} mtrs</span></h6><h6><span>Delay : ${$scope.incidentResults.tm.poi[i].dl} sec</span></h6>`;
                                        $scope.incidentResults.tm.poi[i].contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>JAM</STRONG></span></h6></div><h6><span>${$scope.incidentResults.tm.poi[i].d}</span></h6><h6><span>Length : ${$scope.incidentResults.tm.poi[i].l} mtrs</span></h6><h6><span>Delay : ${$scope.incidentResults.tm.poi[i].dl} sec</span></h6>`;
                                    } else {
                                        $scope.incidentResults.tm.poi[i].dar = 'توقف مروري';
                                        contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>مربى</STRONG></span></h6></div><h6><span>${$scope.incidentResults.tm.poi[i].dar}</span></h6><h6><span>${$scope.incidentResults.tm.poi[i].l} الطول : متر</span></h6><h6><span>${$scope.incidentResults.tm.poi[i].dl} التأخير: ثانية</span></h6>`;
                                        $scope.incidentResults.tm.poi[i].contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>مربى</STRONG></span></h6></div><h6><span>${$scope.incidentResults.tm.poi[i].dar}</span></h6><h6><span>${$scope.incidentResults.tm.poi[i].l} الطول : متر</span></h6><h6><span>${$scope.incidentResults.tm.poi[i].dl} التأخير: ثانية</span></h6>`;
                                    }

                                } else if ($scope.incidentResults.tm.poi[i].d == 'slow traffic') {//حركة مرور بطيئة
                                    $scope.incidentResults.tm.poi[i].img = 'images/car-lorg.png';

                                    if ($scope.selectedLanguage == 'en') {
                                        contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>JAM</STRONG></span></h6></div><h6><span>${$scope.incidentResults.tm.poi[i].d}</span></h6><h6><span>Length : ${$scope.incidentResults.tm.poi[i].l} mtrs</span></h6><h6><span>Delay : ${$scope.incidentResults.tm.poi[i].dl} sec</span></h6>`;
                                        $scope.incidentResults.tm.poi[i].contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>JAM</STRONG></span></h6></div><h6><span>${$scope.incidentResults.tm.poi[i].d}</span></h6><h6><span>Length : ${$scope.incidentResults.tm.poi[i].l} mtrs</span></h6><h6><span>Delay : ${$scope.incidentResults.tm.poi[i].dl} sec</span></h6>`;
                                    } else {
                                        $scope.incidentResults.tm.poi[i].dar = 'حركة مرور بطيئة';
                                        contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>مربى</STRONG></span></h6></div><h6><span>${$scope.incidentResults.tm.poi[i].dar}</span></h6><h6><span>${$scope.incidentResults.tm.poi[i].l} الطول : متر</span></h6><h6><span>${$scope.incidentResults.tm.poi[i].dl} التأخير: ثانية</span></h6>`;
                                        $scope.incidentResults.tm.poi[i].contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>مربى</STRONG></span></h6></div><h6><span>${$scope.incidentResults.tm.poi[i].dar}</span></h6><h6><span>${$scope.incidentResults.tm.poi[i].l} الطول : متر</span></h6><h6><span>${$scope.incidentResults.tm.poi[i].dl} التأخير: ثانية</span></h6>`;
                                    }


                                } else if ($scope.incidentResults.tm.poi[i].d == 'roadworks') {//أعمال الطرق
                                    $scope.incidentResults.tm.poi[i].img = 'images/roadWorks.png';
                                    if ($scope.incidentResults.tm.poi[i].dl == undefined) {
                                        $scope.incidentResults.tm.poi[i].dl = 0;
                                    }

                                    if ($scope.selectedLanguage == 'en') {
                                        contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>ROAD WORKS</STRONG></span></h6></div><h6><span>Length : ${$scope.incidentResults.tm.poi[i].l} mtrs</span></h6><h6 ><span>Delay : ${$scope.incidentResults.tm.poi[i].dl} sec</span></h6>`;
                                        $scope.incidentResults.tm.poi[i].contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>ROAD WORKS</STRONG></span></h6></div><h6><span>Length : ${$scope.incidentResults.tm.poi[i].l} mtrs</span></h6><h6 ><span>Delay : ${$scope.incidentResults.tm.poi[i].dl} sec</span></h6>`;
                                    } else {
                                        $scope.incidentResults.tm.poi[i].dar = 'أعمال الطرق';
                                        contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>أعمال الطرق</STRONG></span></h6></div><h6><span> ${$scope.incidentResults.tm.poi[i].l} الطول : متر</span></h6><h6 ><span>${$scope.incidentResults.tm.poi[i].dl} التأخير: ثانية</span></h6>`;
                                        $scope.incidentResults.tm.poi[i].contentString = contentString = `<div class="jam"><img src="./${$scope.incidentResults.tm.poi[i].img}"  alt="Alternate Text" /><h6><span><STRONG>أعمال الطرق</STRONG></span></h6></div><h6><span> ${$scope.incidentResults.tm.poi[i].l} الطول : متر</span></h6><h6 ><span>${$scope.incidentResults.tm.poi[i].dl} التأخير: ثانية</span></h6>`;
                                    }

                                }

                                var title = $scope.incidentResults.tm.poi[i].d;
                                var icon = $scope.incidentResults.tm.poi[i].img;

                                $scope.drawMarker(map, LatLng, contentString, title, icon);
                                counter++
                            }

                        }
                        $scope.incidentResults.tm.poi = orderBy($scope.incidentResults.tm.poi, $scope.propertyName, true);

                        //alert(counter);
                        setTimeout(function () {
                            $(".nano").nanoScroller();
                            getData = false;
                        }, 1000);
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            } else {
                getData = false;
            }

        }

        function clearMarkers() {

            var markersLayer = map.getLayersByName('Markers')[0];
            markersLayer.clearMarkers();

        }

        function hidePopups() {

            for (var i = 0; i < map.popups.length; i++) {
                if (map.popups[i].visible()) {
                    map.popups[i].hide();
                }
            };
        }

        function clearPopup() {
            var length = map.popups.length;
            for (var i = 0; i < length; i++) {
                if (map.popups[i].visible() == true) {
                    map.popups[i].hide();
                }
            }
        }



        $scope.drawPopup = function (map, latlng, contentString, title, icon) {

            if (map === undefined) {
                map = $scope.map;
            }

            var size = new OpenLayers.Size(155, 105);
            if (title === "closed" || title === "roadworks" || title == 'أعمال الطرق' || title == 'انتظار مروري' || title == 'حركة مرور بطيئة') {
                size = new OpenLayers.Size(175, 105);
            }

            if (this.popup === null || this.popup === undefined) {
                hidePopups();
                this.popup = new OpenLayers.Popup.Anchored(title,
                new OpenLayers.LonLat(latlng.lng, latlng.lat),
                size,
                contentString,
                null,
                true);

                map.addPopup(this.popup);
                this.popup.show();
            } else {

                if (this.popup.visible()) {
                    hidePopups();
                } else {
                    hidePopups();
                    this.popup.show();
                }

            }
            this.popup.autoSize = true;
        }

        $scope.drawMarker = function (map, latlng, contentString, title, icon) {

            var size = new OpenLayers.Size(30, 30);
            var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
            var icon = new OpenLayers.Icon(window.location.origin + '/' + icon, size, offset);
            var marker = new OpenLayers.Marker(new OpenLayers.LonLat(latlng.lng, latlng.lat), icon.clone());
            marker.setOpacity(1);
            marker.events.register('click', marker, function (evt) {


                var size = new OpenLayers.Size(155, 105);
                if (title === "closed" || title === "roadworks" || title == 'أعمال الطرق' || title == 'انتظار مروري' || title == 'حركة مرور بطيئة') {
                    size = new OpenLayers.Size(175, 105);
                }
                if (this.popup === null || this.popup === undefined) {
                    hidePopups();
                    this.popup = new OpenLayers.Popup.Anchored(title,
                    marker.lonlat,
                    size,
                    contentString,
                    null,
                    true);

                    map.addPopup(this.popup);
                    this.popup.show();
                } else {

                    if (this.popup.visible()) {
                        hidePopups();
                    } else {
                        hidePopups();
                        this.popup.show();
                    }


                }
                this.popup.autoSize = true;
            });

            var markersLayer = map.getLayersByName('Markers')[0];
            markersLayer.addMarker(marker);
            map.raiseLayer(markersLayer, map.layers.length);
        }

    }
})();
