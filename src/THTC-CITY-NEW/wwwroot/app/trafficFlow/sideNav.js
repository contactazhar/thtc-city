﻿(function () {
    'use strict';

    angular
        .module('trafficApp')
        .controller('sideNav', sideNav);

    sideNav.$inject = ['$scope', '$http'];

    function sideNav($scope, $http) {
        $scope.title = 'sideNav';
        var basePath = '/';
        $scope.sideNavs = [];

        activate();
        function activate() {
            $scope.selectedLanguage = window.localStorage.getItem('language');
            showStuff();
            $http({
                method: 'POST',
                url: basePath + 'Traffic/getSideNav',
                headers: "application/json"
            }).then(function (data) {
                $scope.sideNavs = data.data;
                if ($scope.selectedLanguage == 'ar') {
                    if (window.location.pathname === "/trafficFlow") {
                        $scope.selectedHeader = $scope.sideNavs[0].navArabic;
                    } else if (window.location.pathname === "/trafficIncidents") {
                        $scope.selectedHeader = $scope.sideNavs[1].navArabic;
                    } else if (window.location.pathname === "/delayHotspots") {
                        $scope.selectedHeader = $scope.sideNavs[2].navArabic;
                    } else if (window.location.pathname === "/weather") {
                        $scope.selectedHeader = $scope.sideNavs[3].navArabic;

                    } else if (window.location.pathname === "/caa") {
                        $scope.selectedHeader = "Create Hotspots";
                    }
                } else {
                    if (window.location.pathname === "/trafficFlow") {
                        $scope.selectedHeader = $scope.sideNavs[0].navEnglish;
                    } else if (window.location.pathname === "/trafficIncidents") {
                        $scope.selectedHeader = $scope.sideNavs[1].navEnglish;
                    } else if (window.location.pathname === "/delayHotspots") {
                        $scope.selectedHeader = $scope.sideNavs[2].navEnglish;
                    } else if (window.location.pathname === "/weather") {
                        $scope.selectedHeader = $scope.sideNavs[3].navEnglish;
                    } else if (window.location.pathname === "/caa") {
                        $scope.selectedHeader = "Create Hotspots";
                    }
                }


                changeLanguage();

            });

        }

        $scope.changeLanguage = function () {
            if ($scope.selectedLanguage === 'ar') {
                $scope.selectedLanguage = 'en';
            } else {
                $scope.selectedLanguage = 'ar';
            }

            changeLanguage();
            window.location.reload();
        }

        function changeLanguage() {

            if ($scope.selectedLanguage === 'ar') {
                $("#english").removeClass("fahrenheit-toggle__option--active");

                $("#arabic").addClass("fahrenheit-toggle__option--active");
                $("#english1").removeClass("fahrenheit-toggle__option--active");

                $("#arabic1").addClass("fahrenheit-toggle__option--active");
                $scope.selectedLanguage = 'ar';
                window.localStorage.setItem('language', $scope.selectedLanguage);

            }
            else {
                $("#arabic").removeClass("fahrenheit-toggle__option--active");
                $("#english").addClass("fahrenheit-toggle__option--active");
                $("#arabic1").removeClass("fahrenheit-toggle__option--active");
                $("#english1").addClass("fahrenheit-toggle__option--active");
                $scope.selectedLanguage = 'en';
                window.localStorage.setItem('language', $scope.selectedLanguage);

            }

        }
        function showStuff() {
            var element = document.getElementById('div1');
            if (window.location.pathname == '/weather' && element != null) {
                document.getElementById('div1').style.display = 'none';
            } else if (element != null) {
                document.getElementById('div1').style.display = 'inline-block';
            }

        }
    }
})();
