﻿(function () {
    'use strict';

    angular
        .module('trafficApp')
        .controller('turnByTurn', turnByTurn);

    turnByTurn.$inject = ['$scope', '$http', 'orderByFilter', '$timeout'];

    function turnByTurn($scope, $http, orderBy, $timeout) {
        $scope.title = 'turnByTurn';
        $scope.selectedTab = 'turnByTurn';
        $scope.sideNav = [];
        var basePath = '/';
        var map;
        var markers = [];
        $scope.showSpinner = true;

        $scope.turnByTurn = [];

        var vectorLayer = new OpenLayers.Layer.Vector("myLayer");

        var voices;
        var voice;
        var voicelist;
        $scope.selected = "ENGLISH";
        var fromProjection = new OpenLayers.Projection("EPSG:4326"); // transform from WGS 1984
        var toProjection = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
        var routeURL = "https://api.tomtom.com/routing/1/calculateRoute/25.1972,55.2744:25.1649,55.4084/json?key=QOtGVndSDrlKfDnYjPKv9pqXDtQCKGZQ&instructionsType=text";
        $scope.languages = ['ENGLISH', 'ARABIC']
        activate();

        $scope.changeLang = function (value) {
            $scope.selected = value;
            $scope.routeResult = {};
            clearInterval($scope.setInt);
            voicelist = responsiveVoice.getVoices();

            if ($scope.selected == 'ARABIC') {
                responsiveVoice.setDefaultVoice("Arabic Female");
                routeURL = "https://api.tomtom.com/routing/1/calculateRoute/25.1972,55.2744:25.1649,55.4084/json?key=QOtGVndSDrlKfDnYjPKv9pqXDtQCKGZQ&instructionsType=text&language=ar";
            } else {
                responsiveVoice.setDefaultVoice("US English Female");
                routeURL = "https://api.tomtom.com/routing/1/calculateRoute/25.1972,55.2744:25.1649,55.4084/json?key=QOtGVndSDrlKfDnYjPKv9pqXDtQCKGZQ&instructionsType=text";
            }
            $scope.getDelayHotspotByYearQuarterAndType();
        }

        function activate() {
            voicelist = responsiveVoice.getVoices();

            if ($scope.selected == 'ARABIC') {
                responsiveVoice.setDefaultVoice("Arabic Female");
                routeURL = "https://api.tomtom.com/routing/1/calculateRoute/25.1972,55.2744:25.1649,55.4084/json?key=QOtGVndSDrlKfDnYjPKv9pqXDtQCKGZQ&instructionsType=text&language=ar";
            } else {
                responsiveVoice.setDefaultVoice("US English Female");
                routeURL = "https://api.tomtom.com/routing/1/calculateRoute/25.1972,55.2744:25.1649,55.4084/json?key=QOtGVndSDrlKfDnYjPKv9pqXDtQCKGZQ&instructionsType=text";
            }


            $scope.selecteLanguage = window.localStorage.getItem('language');
            setTimeout(function () {
                initMap();
                getSideNav();
                $(".nano").nanoScroller();
                getTimeAndDate();
                $scope.getDelayHotspotByYearQuarterAndType();
            }, 1000);


        }


        function getSideNav() {

            $http({
                method: 'POST',
                url: basePath + 'Traffic/getSideNav',
                headers: "application/json"
            }).then(function (data) {
                $scope.sideNav = data.data;

            });
        }

        function getTimeAndDate() {

            var month = new Array();
            month[0] = "Jan";
            month[1] = "Feb";
            month[2] = "Mar";
            month[3] = "Apr";
            month[4] = "May";
            month[5] = "Jun";
            month[6] = "Jul";
            month[7] = "Aug";
            month[8] = "Sep";
            month[9] = "Oct";
            month[10] = "Nov";
            month[11] = "Dec";

            var currentDate = new Date;

            var day = currentDate.getDate();
            var months = month[currentDate.getMonth()];
            var year = currentDate.getFullYear();
            var time = new Date(currentDate.setMinutes(currentDate.getMinutes() - 90)).toLocaleTimeString();

            $scope.currentDate = {};
            $scope.currentDate.day = day;
            $scope.currentDate.month = months;
            $scope.currentDate.year = year;
            $scope.currentDate.time = time;
        }


        function initMap() {
            // create the maps
            var extent = new OpenLayers.Bounds(54.0000, 22.6000, 57.0300, 26.2900).transform(fromProjection, toProjection);
            var offset = 10; // control max zoom-out level

            var options = {
                resolution: 16,
                numZoomLevels: 18 - offset, // control zoom-in level
                maxResolution: 156543.0339 / Math.pow(2, offset),
                maxExtent: new OpenLayers.Bounds(-20037508.3427892, -20037508.3427892, 20037508.3427892, 20037508.3427892),
                projection: "EPSG:900913",
                units: 'm',
                format: 'image/png',
                restrictedExtent: extent,
                eventListeners: {
                    //    "moveend": getIncidentsData,
                    "click": clearPopup
                }
            };

            map = new OpenLayers.Map("map3", options);

            var wms_th = new OpenLayers.Layer.WMS(
                                  "THTC Map WMS",
               "http://www.tracknfind.me/maps",
                  {
                      sphericalMercator: true,
                      layers: 'TCity_UAE',
                      tiled: 'true',
                      format: 'image/png',
                      tilesOrigin: map.maxExtent.left + ',' + map.maxExtent.bottom
                  },
                  {
                      buffer: 0,
                      isBaseLayer: true,

                      displayOutsideMaxExtent: true
                  }
                 );


            var flowUrl = "https://api.tomtom.com/traffic/map/4/tile/flow/relative/${z}/${x}/${y}.png?key=QOtGVndSDrlKfDnYjPKv9pqXDtQCKGZQ&thickness=1";

            var TTFlow = new OpenLayers.Layer.XYZ("Flow Layer", flowUrl, { zoomOffset: offset, isBaseLayer: false });

            var markers = new OpenLayers.Layer.Markers("Markers", { zoomOffset: offset, isBaseLayer: false });

            var labelUrl = "https://api.tomtom.com/map/1/tile/labels/main/${z}/${x}/${y}.png?key=QOtGVndSDrlKfDnYjPKv9pqXDtQCKGZQ";
            var nameLayerTTFlow = new OpenLayers.Layer.XYZ("Label Layer", labelUrl, { zoomOffset: offset, isBaseLayer: false });

            map.addLayers([TTFlow, wms_th, nameLayerTTFlow, markers]);
            map.zoomToMaxExtent();
            map.setCenter(
                new OpenLayers.LonLat(55.2708, 25.2048).transform(fromProjection, toProjection),
                0
            );




            $scope.map = map;

            //debugger
            //var SW = new OpenLayers.LonLat(54.852, 24.449).transform(fromProjection, toProjection);
            //var NE = new OpenLayers.LonLat(55.679, 25.620).transform(fromProjection, toProjection);

        }



        function clearMarkers() {

            var markersLayer = map.getLayersByName('Markers')[0];
            markersLayer.clearMarkers();

        }
        function hidePopups() {

            for (var i = 0; i < map.popups.length; i++) {
                if (map.popups[i].visible()) {
                    map.popups[i].hide();
                }
            };
        }

        function clearPopup() {
            var length = map.popups.length;
            for (var i = 0; i < length; i++) {
                if (map.popups[i].visible() == true) {
                    map.popups[i].hide();
                }
            }
        }

        $scope.drawPopup = function (map, latlng, contentString, title, icon) {

            if (map === undefined) {
                map = $scope.map;
            }

            var size = new OpenLayers.Size(280, 180);

            if (this.popup === null || this.popup === undefined) {
                hidePopups();
                this.popup = new OpenLayers.Popup.Anchored(title,
                new OpenLayers.LonLat(latlng.lng, latlng.lat).transform(fromProjection, toProjection),
                size,
                contentString,
                null,
                true);

                map.addPopup(this.popup);
                this.popup.show();
                map.setCenter(
                new OpenLayers.LonLat(latlng.lng, latlng.lat).transform(fromProjection, toProjection),
                5
            );
            } else {

                if (this.popup.visible()) {
                    hidePopups();
                    map.setCenter(new OpenLayers.LonLat(55.2708, 25.2048).transform(fromProjection, toProjection),
                    2);
                } else {
                    hidePopups();
                    this.popup.show();
                    map.setCenter(
                    new OpenLayers.LonLat(latlng.lng, latlng.lat).transform(fromProjection, toProjection),
                    5
                    );
                }


            }
            this.popup.autoSize = true;
        }

        $scope.drawMarker = function (map, latlng, contentString, title, icon) {

            var size = new OpenLayers.Size(40, 50);
            var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
            var icon = new OpenLayers.Icon(window.location.origin + '/' + icon, 48, offset);
            var marker = new OpenLayers.Marker(new OpenLayers.LonLat(latlng.lng, latlng.lat).transform(fromProjection, toProjection), icon.clone());
            marker.setOpacity(1);
            marker.events.register('click', marker, function (evt) {


                var size = new OpenLayers.Size(280, 180);
                if (this.popup === null || this.popup === undefined) {
                    hidePopups();
                    this.popup = new OpenLayers.Popup.Anchored(title,
                    marker.lonlat,
                    size,
                    contentString,
                    null,
                    true);

                    map.addPopup(this.popup);
                    this.popup.show();
                } else {

                    if (this.popup.visible()) {
                        hidePopups();
                    } else {
                        hidePopups();
                        this.popup.show();
                    }


                }
                this.popup.autoSize = true;
            });

            var markersLayer = map.getLayersByName('Markers')[0];
            markersLayer.addMarker(marker);
            map.raiseLayer(markersLayer, map.layers.length);
        }
        $scope.getQuartersByYear = function (year) {
            $scope.year = {};
            $scope.year.year = year;
            $http({
                method: 'POST',
                url: basePath + 'Traffic/getQuartersByYear',
                headers: "application/json",
                data: $scope.year
            }).then(function (data) {
                $scope.Results = data.data;
                $scope.Quarters = [];
                for (var i = 0; i < $scope.Results.length; i++) {
                    $scope.Quarters.push($scope.Results[i]["0"].quarter);
                }
                $scope.selectedQuarter = $scope.Quarters["0"];

            });
        }


        $scope.getBasesetByYearQuarter = function () {
            //Baseset
            $timeout(function () {
                $scope.getBasesetData = {};
                $scope.getBasesetData.year = $scope.selectedYear;

                $scope.getBasesetData.quarter = $scope.selectedQuarter;
                $scope.getBasesetData.setType = 'BaseSet';
                $http({
                    async: true,
                    crossDomain: true,
                    method: 'POST',
                    url: basePath + 'Traffic/getBasesetByYearQuarter',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: $scope.getBasesetData
                }).then(function (data) {

                    $scope.baseSet = data.data;

                    if ($scope.baseSet.jsonURL === "not exist") {
                        alert("Base Set dosnt exist");
                    } else {


                        $http({
                            crossDomain: true,
                            method: 'GET',
                            url: $scope.baseSet.jsonURL,
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        }).then(function (result) {

                            $scope.baseSetData = result.data;
                            var length = $scope.baseSetData.network.segmentResults.length;

                            $scope.getDelayHotspotByYearQuarterAndType();

                        }).catch(function (error) {
                            //console.log('Cant get data');
                            console.log(error);

                        });
                    }

                }).catch(function (data) {
                    console.log(data);
                });
            }, 1000);

        }


        $scope.getDelayHotspotByYearQuarterAndType = function () {
            var format = new OpenLayers.Format.WKT({
                'internalProjection': map.baseLayer.projection,
                'externalProjection': new OpenLayers.Projection("EPSG:4326")
            });

            vectorLayer.destroyFeatures();
            clearPopup();
            var delayHotspotsLines = [];
            map.setCenter(new OpenLayers.LonLat(55.2708, 25.2048).transform(fromProjection, toProjection), 2);

            $http({
                crossDomain: true,
                method: 'GET',
                url: routeURL,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (result) {
                debugger
                $scope.routeResult = result.data;
                var points = '';
                if ($scope.routeResult.routes["0"].legs["0"].points.length > 1) {
                    for (var j = 0; j < $scope.routeResult.routes["0"].legs["0"].points.length; j++) {
                        if (j < $scope.routeResult.routes["0"].legs["0"].points.length - 1) {
                            var contentString = ``;
                            var LatLng;
                            points = points + $scope.routeResult.routes["0"].legs["0"].points[j].longitude + ' ' + $scope.routeResult.routes["0"].legs["0"].points[j].latitude + ',';
                            if (j == 0) {
                                contentString = `<div class="jam1"><img src="./images/markerImg (2).png"  alt="Alternate Text" /><p class="map-icons1">${j + 1}</p><h6><span><STRONG>BURJ KHALIFA</STRONG></span></h6></div><h6><span>Affected Road Length : xx mtrs</span></h6><h6><span>Average Delay per Vehicle :xx sec</span></h6><h6><span>Average Speed : xx km/h</span></h6><h6><span>Free Flow Speed : xx km/h</span></h6><h6><span>Average Travel Time : xx sec</span></h6><h6><span>Free Flow Travel Time : xx sec</span></h6>`;//<h6><span>Delay Score : ${$scope.comparisionSetData1.network.segmentResults[i].delayScore}</span></h6>
                                LatLng = { lng: $scope.routeResult.routes["0"].legs["0"].points[j].longitude, lat: $scope.routeResult.routes["0"].legs["0"].points[j].latitude }
                                $scope.drawMarker(map, LatLng, contentString, "Start", 'images/markerImg (1).png');
                            }
                        } else {
                            points = points + $scope.routeResult.routes["0"].legs["0"].points[j].longitude + ' ' + $scope.routeResult.routes["0"].legs["0"].points[j].latitude
                            contentString = `<div class="jam1"><img src="./images/markerImg (2).png"  alt="Alternate Text" /><p class="map-icons1">${j + 1}</p><h6><span><STRONG>INTERNATIONAL CITY</STRONG></span></h6></div><h6><span>Affected Road Length : xx mtrs</span></h6><h6><span>Average Delay per Vehicle : xx sec</span></h6><h6><span>Average Speed : xx km/h</span></h6><h6><span>Free Flow Speed : xx km/h</span></h6><h6><span>Average Travel Time : xx sec</span></h6><h6><span>Free Flow Travel Time : xx sec</span></h6>`;//<h6><span>Delay Score : ${$scope.comparisionSetData1.network.segmentResults[i].delayScore}</span></h6>
                            LatLng = { lng: $scope.routeResult.routes["0"].legs["0"].points[j].longitude, lat: $scope.routeResult.routes["0"].legs["0"].points[j].latitude }
                            $scope.drawMarker(map, LatLng, contentString, "Destination", 'images/markerImg (2).png');
                        }

                    }
                }
                if ($scope.selected == "ENGLISH" || $scope.selected == "ARABIC") {
                    var firstFeature = format.read("LINESTRING(" + points + ")");
                    var myFirstLineStyle = OpenLayers.Util.applyDefaults(myFirstLineStyle, OpenLayers.Feature.Vector.style['default']);
                    myFirstLineStyle.strokeColor = "red";
                    myFirstLineStyle.strokeWidth = 8;
                    firstFeature.style = myFirstLineStyle;
                    delayHotspotsLines.push(firstFeature);

                    vectorLayer.addFeatures(delayHotspotsLines);
                    map.addLayer(vectorLayer);
                }

                $scope.showSpinner = false;
                var counter = 0;
                var set = setInterval(function () {

                    if ($scope.routeResult.routes["0"].guidance.instructions[counter].combinedMessage != null || $scope.routeResult.routes["0"].guidance.instructions[counter].combinedMessage != undefined) {

                        var message = $scope.routeResult.routes["0"].guidance.instructions[counter].combinedMessage;
                        responsiveVoice.speak(message);

                    } else {
                        var message = $scope.routeResult.routes["0"].guidance.instructions[counter].message;
                        responsiveVoice.speak(message);
                    }
                    counter++

                    if (counter >= $scope.routeResult.routes["0"].guidance.instructions.length) {
                        clearInterval(set);
                    }
                }, 10000)
                $scope.setInt = set;
            }).catch(function (error) {
                console.log(error);
            });
        }
    }
})();
