﻿(function () {
    'use strict';

    angular
        .module('trafficApp')
        .controller('weatherDubai', weatherDubai);

    weatherDubai.$inject = ['$scope', '$http'];

    function weatherDubai($scope, $http) {
        $scope.title = 'weatherDubai';
        $scope.selectedTab = 'weatherDubai';
        $scope.sideNav = [];
        var basePath = '/';
        $scope.weatherData = {};
        $scope.currentData = {};
        $scope.currentTimeString = "";

        $scope.graphData = [];

        activate();
        $scope.selectedLanguage = window.localStorage.getItem('language');
        $scope.Content = JSON.parse(window.localStorage.getItem('content'));
        $scope.sideNav = JSON.parse(window.localStorage.getItem('sideNav'));

        function activate() {

            setInterval(function () {
                updateClock();
            }, 1000);

            setTimeout(function () {
                if ($scope.selectedLanguage == null || $scope.selectedLanguage == undefined) {
                    window.location.reload();
                }
                getCurrentWeather();
                getOpenWeatherWeek();
                getOpenWeatherHours();
                //startTime();
                
            }, 1000);



        }

        function getCurrentWeather() {
            //http://api.openweathermap.org/data/2.5/weather?q=Dubai&APPID=61b21c19e0c3c90a56b09d06c5d7d968&Unit=Metric
            var api = $scope.Content[28].value;
            var city = $scope.Content[29].value;
            var mode = "json";
            var units = "metric";
            var AppId = "61b21c19e0c3c90a56b09d06c5d7d968";
            var lang = 'ar';
            if ($scope.selectedLanguage == 'en') {
                var url = api + city + "&units=" + units + "&APPID=" + AppId;

            } else {
                var url = api + city + "&units=" + units + "&APPID=" + AppId + "&lang=" + lang;

            }

            $http({
                method: 'GET',
                url: url,
                headers: "application/json"
            }).then(function (data) {
                $scope.currentData = data.data;
                var skycons = new Skycons({ "color": "#f5f5f5" });
                var date = new Date($scope.currentData.dt * 1000);
                var rest = date.toUTCString().split(',');
                rest[1] = rest[1].match(/\b[\w']+(?:[^\w\n]+[\w']+){0,2}\b/g);
                $scope.currentData.dt = rest;
                $scope.currentData.main.temp = Math.ceil($scope.currentData.main.temp);

                var sunRise = new Date($scope.currentData.sys.sunrise * 1000);

                sunRise.setMinutes(sunRise.getMinutes());

                var sunSet = new Date($scope.currentData.sys.sunset * 1000);

                sunSet.setMinutes(sunSet.getMinutes());


                if ($scope.currentTime >= sunRise && $scope.currentTime <= sunSet) {
                    if ($scope.currentData.weather[0].main == "Clear") {

                        skycons.set("icon1", Skycons.CLEAR_DAY);
                        skycons.play();
                    } else if ($scope.currentData.weather[0].main == "Clouds") {
                        skycons.set("icon1", Skycons.PARTLY_CLOUDY_DAY);
                        skycons.play();
                    } else if ($scope.currentData.weather[0].main == "Rain") {
                        skycons.set("icon1", Skycons.RAIN);
                        skycons.play();
                    }
                } else {
                    if ($scope.currentData.weather[0].main == "Clear") {

                        skycons.set("icon1", Skycons.CLEAR_NIGHT);
                        skycons.play();
                    } else if ($scope.currentData.weather[0].main == "Clouds") {
                        skycons.set("icon1", Skycons.PARTLY_CLOUDY_NIGHT);
                        skycons.play();
                    } else if ($scope.currentData.weather[0].main == "Rain") {
                        skycons.set("icon1", Skycons.RAIN);
                        skycons.play();
                    }
                }




            });
        }

        function getOpenWeatherHours() {
            //http://api.openweathermap.org/data/2.5/forecast?q=Dubai&mode=json&AppId=61b21c19e0c3c90a56b09d06c5d7d968&Unit=Metric

            var api = $scope.Content[30].value;
            var city = $scope.Content[29].value;
            var mode = "json";
            var units = "metric";
            var AppId = "61b21c19e0c3c90a56b09d06c5d7d968";
            var lang = 'ar';
            if ($scope.selectedLanguage == 'en') {
                var url = api + city + "&mode=" + mode + "&units=" + units + "&cnt=12&APPID=" + AppId;
            } else {
                var url = api + city + "&mode=" + mode + "&units=" + units + "&cnt=12&APPID=" + AppId + "&lang=" + lang;
            }


            $http({
                method: 'GET',
                url: url,
                headers: "application/json"
            }).then(function (data) {

                $scope.hoursData = data.data;
                var count = data.data.cnt;

                for (var i = 0; i < count; i++) {

                    var date = new Date($scope.hoursData.list[i].dt * 1000);
                    var rest = date.toUTCString().split(',');
                    rest[1] = rest[1].match(/\b[\w']+(?:[^\w\n]+[\w']+){0,2}\b/g);
                    $scope.hoursData.list[i].dt = rest;

                    date.setMinutes(date.getMinutes());
                    var time = date.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
                    var temp = Math.round($scope.hoursData.list[i].main.temp_max);
                    var graphData = { time: time, temp: temp };
                    $scope.graphData.push(graphData);

                }
                setTimeout(function () {
                    morrisGraph();
                }, 1000);
            });
        }
        function getOpenWeatherWeek() {

            //http://api.openweathermap.org/data/2.5/forecast/daily?q=Dubai&mode=json&units=metric&cnt=8&APPID=61b21c19e0c3c90a56b09d06c5d7d968

            var api = $scope.Content[31].value;
            var city = $scope.Content[29].value;
            var mode = "json";
            var units = "metric";
            var AppId = "61b21c19e0c3c90a56b09d06c5d7d968";
            var lang = 'ar';
            if ($scope.selectedLanguage == 'en') {

                var url = api + city + "&mode=" + mode + "&units=" + units + "&cnt=8&APPID=" + AppId;
            } else {
                var url = api + city + "&mode=" + mode + "&units=" + units + "&cnt=8&APPID=" + AppId + "&lang=" + lang;
            }

            $http({
                method: 'GET',
                url: url,
                headers: "application/json"
            }).then(function (data) {
                $scope.weatherData = data.data;
                var count = data.data.cnt;
                var skycons = new Skycons({ "color": "#f5f5f5" });
                for (var i = 0; i < count; i++) {

                    var date = new Date($scope.weatherData.list[i].dt * 1000);
                    var rest = date.toUTCString().split(',');
                    rest[1] = rest[1].match(/\b[\w']+(?:[^\w\n]+[\w']+){0,2}\b/g);
                    $scope.weatherData.list[i].dt = rest;

                    $scope.weatherData.list[i].iconId = "icons" + i;
                }

                setTimeout(function () {
                    for (var i = 0; i < count; i++) {
                        if ($scope.weatherData.list[i].weather[0].main === "Clear") {
                            skycons.set("icons" + i, Skycons.CLEAR_DAY);
                            skycons.play();
                        } else if ($scope.weatherData.list[i].weather[0].main === "Rain") {
                            skycons.set("icons" + i, Skycons.RAIN);
                            skycons.play();
                        } else if ($scope.weatherData.list[i].weather[0].main === "Clouds") {
                            skycons.set("icons" + i, Skycons.PARTLY_CLOUDY_DAY);
                            skycons.play();
                        }
                    }
                })


            });
        }

        function getSideNav() {

            $http({
                method: 'POST',
                url: basePath + 'Traffic/getSideNav',
                headers: "application/json"
            }).then(function (data) {
                $scope.sideNav = data.data;
                window.localStorage.setItem('showList', JSON.stringify($scope.sideNav[36]));
                window.localStorage.setItem('hideList', JSON.stringify($scope.sideNav[37]));

            });
        }

        function updateClock() {
            $scope.$apply(function () {
                $scope.currentTime = new Date();
                //$scope.currentTime = new Date($scope.currentTime.setMinutes($scope.currentTime.getMinutes() - 90));
                $scope.currentTimeString = $scope.currentTime.toLocaleTimeString();
            })
        }

        function morrisGraph() {
            if (true) {
                var temp = [$scope.sideNav[57].navEnglish];
            } else {
                var temp = [$scope.sideNav[57].navArabic];
            }

            Morris.Area({
                element: 'graph16',
                data: $scope.graphData,
                xkey: "time",
                ykeys: ["temp"],
                labels: temp,
                numLines: 5,
                yLabelFormat: function (y) {
                    return y.toString() + '°C';
                },
                hideHover: true,
                smooth: true,
                parseTime: false
            });

        }

    }
})();
