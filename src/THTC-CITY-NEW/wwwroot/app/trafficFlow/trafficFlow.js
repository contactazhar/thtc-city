﻿(function () {
    'use strict';

    angular
        .module('trafficApp')
        .controller('trafficFlow', trafficFlow);

    trafficFlow.$inject = ['$scope', '$http'];

    function trafficFlow($scope, $http) {
        var basePath = '/';
        $scope.title = 'trafficFlow';
        $scope.latLngValues = {};
        $scope.trafficFlowData = {};
        $scope.viewGraph = false;
        var map;
        $scope.LatLng = {};
        $scope.LatLng.lat = 25.429723;
        $scope.LatLng.lng = 55.4323196;
        $scope.LatLng.zoom = 10;
        $scope.avgCalculator = "low";
        $scope.avgSpeed = 0;
        $scope.travelTime = {};
        $scope.sideNav = [];
        $scope.LatLngIncident = [];
        $scope.liveTrafficData = {};
        $scope.showSpinner = true;
        $scope.showSpinner1 = true;
        $scope.incidentResults = [];
        $scope.liveTrafficData.LatLng = [];
        $scope.lastUpdatedData = '';
        $scope.totalCount = 0;
        $scope.optimalSpeed = 42;
        $scope.Content = [];

        //$scope.liveAreaCovered = 8097;

        var fromProjection = new OpenLayers.Projection("EPSG:4326"); // transform from WGS 1984
        var toProjection = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection

        var liveReload = null;

        activate();

        function activate() {
            $scope.selectedLanguage = window.localStorage.getItem('language');

            getSideNav();
            getDBCOntent();
            setTimeout(function () {
                if ($scope.selectedLanguage == null || $scope.selectedLanguage == undefined) {
                    window.location.reload();
                }
                initMap();
                getTimeAndDate();
                getAreaCovered();
            }, 1000);

        }
        function getTimeAndDate() {

            var month = new Array();
            month[0] = "Jan";
            month[1] = "Feb";
            month[2] = "Mar";
            month[3] = "Apr";
            month[4] = "May";
            month[5] = "Jun";
            month[6] = "Jul";
            month[7] = "Aug";
            month[8] = "Sep";
            month[9] = "Oct";
            month[10] = "Nov";
            month[11] = "Dec";

            var currentDate = new Date;

            var day = currentDate.getDate();
            var months = month[currentDate.getMonth()];
            var year = currentDate.getFullYear();
            // var time = new Date(currentDate.setMinutes(currentDate.getMinutes() - 90)).toLocaleTimeString();

            var time = window.localStorage.getItem("updatedTime");

            $scope.currentDate = {};
            $scope.currentDate.day = day;
            $scope.currentDate.month = months;
            $scope.currentDate.year = year;
            $scope.currentDate.time = time;

        }

        function initMap() {


            var extent = new OpenLayers.Bounds(Number($scope.Content[0].value), Number($scope.Content[1].value), Number($scope.Content[2].value), Number($scope.Content[3].value)).transform(fromProjection, toProjection);
            var offset = Number($scope.Content[4].value); // control max zoom-out level

            var options = {
                resolution: Number($scope.Content[5].value),
                numZoomLevels: Number($scope.Content[6].value) - offset, // control zoom-in level
                maxResolution: Number($scope.Content[7].value) / Math.pow(2, offset),
                maxExtent: new OpenLayers.Bounds(Number($scope.Content[8].value), Number($scope.Content[9].value), Number($scope.Content[10].value), Number($scope.Content[11].value)),
                projection: "EPSG:900913",
                units: 'm',
                format: $scope.Content[15].value,
                restrictedExtent: extent,
            };

            map = new OpenLayers.Map("map", options);

            if ($scope.selectedLanguage == 'en') {
                var layers = $scope.Content[13].value;
            } else {
                var layers = $scope.Content[48].value;
            }


            var wms_th = new OpenLayers.Layer.WMS(
                $scope.Content[12].value,
                $scope.Content[25].value,
                  {
                      sphericalMercator: true,
                      layers: layers,
                      tiled: $scope.Content[14].value,
                      format: $scope.Content[15].value,
                      tilesOrigin: map.maxExtent.left + ',' + map.maxExtent.bottom
                  },
                  {
                      buffer: 0,
                      isBaseLayer: true,

                      displayOutsideMaxExtent: true
                  }
                 );

            var url = $scope.Content[16].value;
            var TTFlow = new OpenLayers.Layer.XYZ("Flow Layer", url, { zoomOffset: offset, isBaseLayer: false });

            var titleLayer = $scope.Content[17].value;
            if ($scope.selectedLanguage == 'en') {
                var nameLayerTTFlow = new OpenLayers.Layer.XYZ("Flow Layer", titleLayer, { zoomOffset: offset, isBaseLayer: false });
                map.addLayers([TTFlow, wms_th, nameLayerTTFlow]);
            } else {
                map.addLayers([TTFlow, wms_th]);
            }



            map.zoomToMaxExtent();
            map.setCenter(
                new OpenLayers.LonLat($scope.Content[18].value, $scope.Content[19].value).transform(fromProjection, toProjection),
                Number($scope.Content[49].value)
            );

            map.events.register('click', map, handleMapClick);


        }
        function handleMapClick(evt) {
            var lonlat = map.getLonLatFromPixel(evt.xy).transform(toProjection, fromProjection);

            var zoom = map.getZoom() + 9;

            $http({
                method: 'GET',
                url: $scope.Content[20].value + zoom + $scope.Content[21].value + lonlat.lat + ',' + lonlat.lon,
                headers: "application/json"
            }).then(function (data) {
                if (data.status === 200) {
                    var freeFlowData = data.data;
                    var popup;
                    if ($scope.selectedLanguage == 'en') {
                        var contentString = `<h6><span>${$scope.sideNav[48].navEnglish} : ${freeFlowData.flowSegmentData.coordinates.coordinate["0"].latitude}</span></h6><h6><span>${$scope.sideNav[49].navEnglish} : ${freeFlowData.flowSegmentData.coordinates.coordinate["0"].longitude}</span></h6><h6><span>${$scope.sideNav[50].navEnglish} : ${freeFlowData.flowSegmentData.currentSpeed} Kmph</span></h6><h6><span>${$scope.sideNav[51].navEnglish} : ${freeFlowData.flowSegmentData.currentTravelTime} Sec</span></h6><h6><span>${$scope.sideNav[52].navEnglish} : ${freeFlowData.flowSegmentData.freeFlowSpeed} Kmph</span></h6><h6><span>${$scope.sideNav[53].navEnglish} : ${freeFlowData.flowSegmentData.freeFlowTravelTime} Sec</span></h6>`;
                    } else {
                        var contentString = `<h6><span>${freeFlowData.flowSegmentData.coordinates.coordinate["0"].latitude} : ${$scope.sideNav[48].navArabic}</span></h6><h6><span>${freeFlowData.flowSegmentData.coordinates.coordinate["0"].longitude} : ${$scope.sideNav[49].navArabic}</span></h6><h6><span>${freeFlowData.flowSegmentData.currentSpeed}  ${$scope.sideNav[50].navArabic}</span></h6><h6><span>${freeFlowData.flowSegmentData.currentTravelTime} ${$scope.sideNav[51].navArabic}</span></h6><h6><span>${freeFlowData.flowSegmentData.freeFlowSpeed} ${$scope.sideNav[52].navArabic}</span></h6><h6><span>${freeFlowData.flowSegmentData.freeFlowTravelTime} ${$scope.sideNav[53].navArabic}</span></h6>`;
                    }

                    var size = new OpenLayers.Size(245, 115);

                    if (popup === null || popup === undefined) {
                        hidePopups();
                        popup = new OpenLayers.Popup.Anchored('Details',
                        new OpenLayers.LonLat(lonlat.lon, lonlat.lat).transform(fromProjection, toProjection),
                        size,
                        contentString,
                        null,
                        true);

                        map.addPopup(popup);
                        popup.show();
                    } else {

                        if (popup.visible()) {
                            hidePopups();
                        } else {
                            hidePopups();
                            popup.show();
                        }

                    }
                    popup.autoSize = true;
                }
            }).catch(function (data) {
                if (data.status === 400) {
                    console.log('cant get data.');

                    hidePopups();
                    if ($scope.selectedLanguage == 'en') {
                        alert($scope.sideNav[54].navEnglish)
                    } else {
                        alert($scope.sideNav[54].navArabic);
                    }


                }
            });
        }
        function hidePopups() {

            for (var i = 0; i < map.popups.length; i++) {
                if (map.popups[i].visible()) {
                    map.popups[i].hide();
                }
            };
        }
        function getSideNav() {

            $http({
                method: 'POST',
                url: basePath + 'Traffic/getSideNav',
                headers: "application/json"
            }).then(function (data) {
                $scope.sideNav = data.data;
                setTimeout(function () {
                    $(".nano").nanoScroller();
                }, 1000);

                window.localStorage.setItem('showList', JSON.stringify($scope.sideNav[36]));
                window.localStorage.setItem('hideList', JSON.stringify($scope.sideNav[37]));

                window.localStorage.setItem('sideNav', JSON.stringify($scope.sideNav));
            }).catch(function (error) {
                console.log(error)
            });;
        }

        function getDBCOntent() {

            $http({
                method: 'POST',
                url: basePath + 'Traffic/contentManagement',
                headers: "application/json"
            }).then(function (data) {
                $scope.Content = data.data;
                window.localStorage.setItem('content', JSON.stringify($scope.Content));
            }).catch(function (error) {
                console.log(error)
            });;
        }

        function getAreaCovered() {
            $http({
                method: 'GET',
                url: $scope.Content[22].value + $scope.Content[24].value + $scope.Content[23].value,
            }).then(function (data) {

                $scope.liveAreaCovered = Math.round(data.data.roadsLengthMeters / 1000);

            }).catch(function (error) {
                console.log(error);
            });

            setTimeout(function () {
                $('.count').each(function () {
                    $(this).prop('Counter', 0).animate({
                        Counter: $(this).text()
                    }, {
                        duration: 4000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });
            }, 2000);
        }
    }

})();
