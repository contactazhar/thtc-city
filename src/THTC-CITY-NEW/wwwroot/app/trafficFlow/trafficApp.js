﻿var mainpageModule = angular.module('trafficApp', ['share', 'chart.js'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider.when('/trafficFlow', { templateUrl: '/app/trafficFlow/trafficFlow.html', caseInsensitiveMatch: true });
        $routeProvider.when('/trafficIncidents', { templateUrl: '/app/trafficFlow/trafficIncidents.html', caseInsensitiveMatch: true });
        $routeProvider.when('/delayHotspots', { templateUrl: '/app/trafficFlow/delayHotspots.html', caseInsensitiveMatch: true });
        $routeProvider.when('/liveTrafficUpdates', { templateUrl: '/app/trafficFlow/liveTrafficUpdates.html', caseInsensitiveMatch: true });
        $routeProvider.when('/weather', { templateUrl: '/app/trafficFlow/weather.html', caseInsensitiveMatch: true });
        $routeProvider.when('/caa', { templateUrl: '/app/trafficFlow/CreateCAA.html', caseInsensitiveMatch: true });

        //$routeProvider.when('/profile', { templateUrl: '/app/profile/index.html', caseInsensitiveMatch: true });


        //$routeProvider.when('/mainpage/editProfile', { templateUrl: '/app/mainpage/editProfile.html', caseInsensitiveMatch: true });
        //$routeProvider.when('/mainpage/myfeeds', { templateUrl: '/app/mainpage/viewMyFeeds.html', caseInsensitiveMatch: true });
        //$routeProvider.when('/mainpage/followedFeeds', { templateUrl: '/app/mainpage/followedFeeds.html', caseInsensitiveMatch: true });

        $routeProvider.otherwise({ redirectTo: '/trafficFlow' });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });