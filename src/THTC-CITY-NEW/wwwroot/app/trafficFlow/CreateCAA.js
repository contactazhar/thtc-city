﻿(function () {
    'use strict';

    angular
        .module('trafficApp')
        .controller('createCAA', createCAA);

    createCAA.$inject = ['$scope', '$http', 'orderByFilter', '$mdDialog'];

    function createCAA($scope, $http, orderBy, $mdDialog) {
        $scope.title = 'createCAA';
        $scope.selectedTab = 'createCAA';
        $scope.sideNav = [];
        var basePath = '/';
        $scope.caa = {};
        $scope.storeDelayHotspots = {};

        activate();

        function activate() {
            $scope.Content = JSON.parse(window.localStorage.getItem('content'));
            $scope.sideNav = JSON.parse(window.localStorage.getItem('sideNav'));
            setTimeout(function () {
                getSideNav();
                $(".nano").nanoScroller();
            }, 1000);


        }


        function getSideNav() {

            $http({
                method: 'POST',
                url: basePath + 'Traffic/getSideNav',
                headers: "application/json"
            }).then(function (data) {
                $scope.sideNav = data.data;

            });

            $http({
                method: 'GET',
                url: basePath + 'Traffic/getCAAWithoutURLs',
                headers: "application/json"
            }).then(function (data) {
                $scope.caaWithoutUrls = data.data;

            });
        }
        $scope.createCAA = function (caaForm) {

            //https://api.tomtom.com/traffic/trafficstats/status/1/191793?key=rgF7oOVbWn0EOjdBJnq1U0nWA0bJsIP3
            //https://api.tomtom.com/traffic/trafficstats/caa/1?key=rgF7oOVbWn0EOjdBJnq1U0nWA0bJsIP3

            if (caaForm.$valid) {
                var from = null;
                var to = null;
                var time = null;
                if ($scope.caa.setType == 'BaseSet') {
                    time = "01:00-03:00";
                } else if ($scope.caa.setType == 'Morning') {
                    time = "06:00-09:59";
                } else if ($scope.caa.setType == 'Afternoon') {
                    time = "13:00-14:59";
                } else {
                    time = "17:00-20:59";
                }


                if ($scope.caa.quarter == 'Q1') {
                    from = $scope.caa.year + '-01-01';
                    to = $scope.caa.year + "-03-31";
                } else if ($scope.caa.quarter == 'Q2') {
                    from = $scope.caa.year + '-04-01';
                    to = $scope.caa.year + "-06-30";
                } else if ($scope.caa.quarter == 'Q3') {
                    from = $scope.caa.year + '-07-01';
                    to = $scope.caa.year + "-09-30";
                } else {
                    from = $scope.caa.year + '-10-01';
                    to = $scope.caa.year + "-12-31";
                }
                $scope.jobPostData = {
                    "jobName": $scope.caa.jobName + "_" + $scope.caa.setType,
                    "distanceUnit": "KILOMETERS",
                    "network": {
                        "name": "THTC Bounding Box",
                        "boundingBox": {
                            "leftDownCorner": {
                                "longitude": $scope.Content[44].value,
                                "latitude": $scope.Content[45].value
                            },
                            "rightTopCorner": {
                                "longitude": $scope.Content[46].value,
                                "latitude": $scope.Content[47].value
                            }
                        },
                        "timeZoneId": "UTC",
                        "frcs": [
                          "0",
                          "1",
                          "2",
                          "3",
                          "4",
                          "5"
                        ],
                        "probeSource": "ALL"
                    },
                    "dateRange": {
                        "name": $scope.caa.quarter,

                        "from": from,
                        "to": to
                    },
                    "timeSets": [
                      {
                          "name": "All Days",
                          "timeGroups": [
                            {
                                "days": [
                                  "MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"
                                ],
                                "times": [
                                  time

                                ]
                            }
                          ]
                      }
                    ]
                };

                $http({
                    async: true,
                    crossDomain: true,
                    method: 'POST',
                    url: 'https://api.tomtom.com/traffic/trafficstats/caa/1?key=rgF7oOVbWn0EOjdBJnq1U0nWA0bJsIP3',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: $scope.jobPostData
                }).then(function (data) {

                    $scope.jobData = data.data;

                    $scope.showAlert($scope.caa, $scope.jobData, 'caa');

                    storeDelayHotspots($scope.caa.year, $scope.caa.quarter, $scope.caa.setType, $scope.jobData);

                    caaForm.$setPristine();
                    caaForm.$setUntouched();

                    $scope.caa = {};
                    $scope.jobData = {};
                    activate();


                }).catch(function (data) {
                    //console.log('Cant get data');
                    console.log(data);

                });

            }

        }

        $scope.showAlert = function (caaData, job, fromWhere) {
            var message;
            if (fromWhere == 'caa') {
                message = caaData.setType + ' set for year: ' + caaData.year + ' quarter: ' + caaData.quarter + ' is created with job ID: ' + job.jobId;
            } else {
                message = caaData.setType + ' set for year: ' + caaData.year + ', quarter: ' + caaData.quarter + ' report result is stored in Database with job ID: ' + job.jobId;
            }

            $mdDialog.show(
              $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(false)
                .title('Success')
                .textContent(message)
                .ariaLabel('Alert Dialog Demo')
                .ok('Ok')
            );
        };

        function DialogController($scope, $mdDialog) {
            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        }

        $scope.getCAAResult = function (job) {
            $http({
                async: true,
                crossDomain: true,
                method: 'GET',
                url: 'https://api.tomtom.com/traffic/trafficstats/status/1/' + job.jobId + '?key=rgF7oOVbWn0EOjdBJnq1U0nWA0bJsIP3',
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (data) {
                $scope.jobResult = {};
                $scope.jobResult = data.data;

                if (!$scope.jobResult.jobState) {
                    storeDelayHotspots(job.year, job.quarter, job.setType, $scope.jobResult);
                    $scope.showAlert(job, $scope.jobResult, 'status');

                    activate();
                }


            }).catch(function (data) {
                //console.log('Cant get data');
                console.log(data);

            });
        }

        function storeDelayHotspots(year, quarter, setType, job) {

            $scope.storeDelayHotspots.year = year;
            $scope.storeDelayHotspots.quarter = quarter;
            $scope.storeDelayHotspots.jobId = job.jobId;
            $scope.storeDelayHotspots.setType = setType;
            if (job.urls != undefined || job.urls != null) {
                $scope.storeDelayHotspots.jsonURL = job.urls[0];
                $scope.storeDelayHotspots.zipURL = job.urls[1];
            }


            $http({
                async: true,
                crossDomain: true,
                method: 'POST',
                url: basePath + 'Traffic/storeDelayHotspots',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.storeDelayHotspots
            }).then(function (data) {
                debugger

                $scope.jobData = data.data;

                if ($scope.jobData == "exist") {
                    alert("Alreay exist");
                }
                $scope.storeDelayHotspots = {};
            }).catch(function (data) {
                //console.log('Cant get data');
                console.log(data);

            });
        }


    }
})();
