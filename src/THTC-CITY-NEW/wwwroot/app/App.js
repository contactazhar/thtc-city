﻿var commonModule = angular.module('share',
    [
      'ngAnimate',        // animations
        'ngRoute',          // routing
        //'ngSanitize',       // sanitizes html bindings (ex: sidebar.js)
        'ngMaterial',
        // Custom modules
       'common',// common functions, logger, spinner
       "chart.js",

       // 'common.bootstrap', // bootstrap dialog wrapper functions
       // // 3rd Party Modules        
       //'angular-loading-bar',
       // 'ui.bootstrap',      // ui-bootstrap (ex: carousel, pagination, dialog),
       // //'data-table',
       // 'md.data.table',
       // 'ngFileUpload',
    ]);




var mainModule = angular.module('main', ['share']);

commonModule.factory('validator', function () { return valJs.validator(); });

mainModule.controller("indexViewModel",
function ($scope, $http, $q, $window, $location, $mdDialog, config, $rootScope) {
    $scope.showList = " ";
    $scope.hideList = " ";
    activate();
    function activate() {
        $scope.selectedLanguage = window.localStorage.getItem('language');
        $scope.showList = JSON.parse(window.localStorage.getItem('showList'));
        $scope.hideList = JSON.parse(window.localStorage.getItem('hideList'));
        if ($scope.selectedLanguage == 'en') {

            
            $scope.showList = $scope.showList.navEnglish;
            $scope.hideList = $scope.hideList.navEnglish;
        } else {
            
            $scope.showList = $scope.showList.navArabic;
            $scope.hideList = $scope.hideList.navArabic;
        }
       
    }

});

