﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace THTC_CITY_NEW.Models
{
    public class IncidentDecodedCoOrds
    {
        public int Id { get; set; }
        public string OpenLrBinary { get; set; }
        public string OpenLrId { get; set; }
        public string LongLat1 { get; set; }
        public string LongLat2 { get; set; }
        public string PName1 { get; set; }
        public string PName2 { get; set; }
    }
}
