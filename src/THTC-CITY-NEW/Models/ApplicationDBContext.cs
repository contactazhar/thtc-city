﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace THTC_CITY_NEW.Models
{
    public class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options)
          : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<Test> Test { get; set; }

        public DbSet<SideNavs> SideNavs { get; set; }

        public DbSet<LiveTraffic> LiveTraffic { get; set; }

        public DbSet<FlowDecodedCoOrds> FlowDecodedCoOrds { get; set; }

        public DbSet<IncidentDecodedCoOrds> IncidentDecodedCoOrds { get; set; }
        public DbSet<DelayHotspots> DelayHotspots { get; set; }
        public DbSet<ContentManagement> ContentManagement { get; set; }
    }
}
