﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace THTC_CITY_NEW.Models
{
    [Table("Test")]
    public class Test
    {
        public int Id { get; set; }
        [Required]
        [Key]
        public string TestId { get; set; }
        public string MainTag { get; set; }
        public string Tag1 { get; set; }
        public string Tag2 { get; set; }
        public string Tag3 { get; set; }
        public string UserBiography { get; set; }
    }
}
