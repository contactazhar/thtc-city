﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace THTC_CITY_NEW.Models
{
    public class SideNavs
    {
        public int Id { get; set; }

        public string NavEnglish { get; set; }

        public string NavArabic { get; set; }
    }
}
