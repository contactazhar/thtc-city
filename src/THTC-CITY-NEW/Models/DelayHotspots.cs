﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace THTC_CITY_NEW.Models
{
    public class DelayHotspots
    {
        public int Id { get; set; }

        public string Year { get; set; }

        public string Quarter { get; set; }

        public string SetType { get; set; }

        public int JobId { get; set; }

        public string JsonURL { get; set; }

        public string ZipURL { get; set; }
    }
}
